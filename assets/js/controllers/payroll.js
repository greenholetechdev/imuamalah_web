var Payroll = {
 module: function () {
  return 'payroll';
 },

 add: function () {
  window.location.href = url.base_url(Payroll.module()) + "add";
 },

 back: function () {
  window.location.href = url.base_url(Payroll.module()) + "index";
 },

 search: function (elm, e) {
  if (e.keyCode == 13) {
   var keyWord = $(elm).val();
   if (keyWord != '') {
    window.location.href = url.base_url(Payroll.module()) + "search" + '/' + keyWord;
   } else {
    window.location.href = url.base_url(Payroll.module()) + "index";
   }
  }
 },

 getPostDataItem: function () {
  var tb_data = $('table#tb_item').find('tbody').find('tr');
  var data = [];
  $.each(tb_data, function () {
   var td = $(this).find('td');
   if (td.length > 1) {
    data.push({
     'payroll_category': $(this).find('td:eq(0)').find('select').val(),
     'jumlah': $(this).find('td:eq(1)').find('input').val(),
     'keterangan': $(this).find('td:eq(2)').find('textarea').val(),
    });
   }
  });

  return data;
 },

 getPostData: function () {
  var data = {
   'id': $('#id').val(),
   'pegawai': $('#pegawai').val(),
   'tanggal_faktur': $('#tanggal_faktur').val(),
   'tanggal_bayar': $('#tanggal_bayar').val(),
   'periode': $('#periode').val(),
   'keterangan': $('#keterangan').val(),
   'jumlah': $('label#total').text(),
   'data_item': Payroll.getPostDataItem()
  };

  return data;
 },

 simpan: function (id) {
  var data = Payroll.getPostData();
  var formData = new FormData();
  formData.append('data', JSON.stringify(data));
  formData.append("id", id);

  if (validation.run()) {
   $.ajax({
    type: 'POST',
    data: formData,
    dataType: 'json',
    processData: false,
    contentType: false,
    async: false,
    url: url.base_url(Payroll.module()) + "simpan",
    error: function () {
     toastr.error("Gagal");
     message.closeLoading();
    },

    beforeSend: function () {
     message.loadingProses("Proses Simpan...");
    },

    success: function (resp) {
     if (resp.is_valid) {
      toastr.success("Berhasil Disimpan");
      var reload = function () {
       window.location.href = url.base_url(Payroll.module()) + "detail" + '/' + resp.id;
      };

      setTimeout(reload(), 1000);
     } else {
      toastr.error("Gagal Disimpan");
     }
     message.closeLoading();
    }
   });
  }
 },

 ubah: function (id) {
  window.location.href = url.base_url(Payroll.module()) + "ubah/" + id;
 },

 detail: function (id) {
  window.location.href = url.base_url(Payroll.module()) + "detail/" + id;
 },

 setThousandSparator: function () {
  $('#jumlah').divide({
   delimiter: '.',
   divideThousand: true
  });
 },

 delete: function (id) {
  $.ajax({
   type: 'POST',
   dataType: 'json',
   async: false,
   url: url.base_url(Payroll.module()) + "delete/" + id,

   error: function () {
    toastr.error("Gagal Dihapus");
   },

   success: function (resp) {
    if (resp.is_valid) {
     toastr.success("Berhasil Dihapus");
     var reload = function () {
      window.location.href = url.base_url(Payroll.module()) + "index";
     };

     setTimeout(reload(), 1000);
    } else {
     toastr.error("Gagal Dihapus");
    }
   }
  });
 },

 setSelect2: function () {
  $("#pegawai").select2();
  $("#periode").select2();
 },

 setDate: function () {
  $('input#tanggal_faktur').datepicker({
   dateFormat: 'yy-mm-dd'
  });
  $('input#tanggal_bayar').datepicker({
   dateFormat: 'yy-mm-dd'
  });
  $('input#tanggal_faktur_fb').datepicker({
   dateFormat: 'yy-mm-dd'
  });
  $('input#tanggal_bayar_fb').datepicker({
   dateFormat: 'yy-mm-dd'
  });
 },

 addItem: function (elm, e) {
  e.preventDefault();
  var tr = $(elm).closest('tbody').find('tr:last');
  $.ajax({
   type: 'POST',
   dataType: 'html',
   data: {
    index: tr.index()
   },
   async: false,
   url: url.base_url(Payroll.module()) + "addItem",
   error: function () {
    toastr.error("Gagal");
   },

   beforeSend: function () {

   },

   success: function (resp) {
    var tr = $(elm).closest('tbody').find('tr:last');
    var newTr = tr.clone();
    tr.html(resp);
    tr.after(newTr);
   }
  });
 },

 hitungTotal: function () {
  var tb_product = $('table#tb_item').find('tbody').find('tr');
//  console.log(tb_product);
  var total = 0;
  $.each(tb_product, function () {
   var td = $(this).find('td');
   if (td.length > 1) {
    if (!$(this).hasClass('deleted')) {
     var sub_total = $(this).find('td:eq(1)').find('input').val().toString();
     if (sub_total != '') {
      sub_total = sub_total.replace(',', '');
      var category = $(this).find('td:eq(0)').find('select').val();
      var jenis = $(this).find('td:eq(0)').find('select option[value="' + category + '"]').attr('action');
      sub_total = parseInt(sub_total);
      if (jenis == '+') {
       sub_total *= 1;
      } else {
       sub_total *= -1;
      }
      total += sub_total;
     }
    }
   }
  });

  $('label#total').text(total);
  $('label#total').divide({
   delimiter: '.',
   divideThousand: true
  });
 },

 deleteItem: function (elm) {
  var data_id = $(elm).closest('tr').attr('data_id');
  if (data_id != '') {
   var tr = $('table#tb_product').find('tbody').find('tr[data_id="' + data_id + '"]');
   var metode = tr.find('td:eq(2)').find('select').val();
   tr.addClass('hide');
   tr.addClass('deleted');
   if (metode == 2) {
    tr.next().addClass('hide');
    tr.next().addClass('deleted');
   }
  } else {
   $(elm).closest('tr').remove();
  }

  Payroll.hitungTotal();
 },

 getMetodeBayar: function (elm) {
  var metode = $(elm).val();
  var index = $(elm).closest('tr').index();
  switch (metode) {
   case "2":
    $.ajax({
     type: 'POST',
     data: {
      metode: metode,
      index: index
     },
     dataType: 'html',
     async: false,
     url: url.base_url(Payroll.module()) + "getMetodeBayar",
     error: function () {
      toastr.error("Gagal");
     },

     beforeSend: function () {

     },

     success: function (resp) {
      bootbox.dialog({
       message: resp
      });
     }
    });
    break;

   default:

    break;
  }
 },

 pilihBank: function (elm) {
  message.closeDialog();
  var index_row = $(elm).attr('index_row');
  var tb_product = $('table#tb_product').find('tbody');
  var tr_product = tb_product.find('tr:eq(' + index_row + ')');

  var nama_bank = $(elm).closest('tr').find('td:eq(0)').text();
  var akun = $(elm).closest('tr').find('td:eq(2)').text();
  var no_rek = $(elm).closest('tr').find('td:eq(1)').text();
  var data_id_bank = $(elm).closest('tr').attr('data_id');
  var tr_bank = '<tr data_bank="' + data_id_bank + '">';
  tr_bank += '<td colspan="7">';
  tr_bank += '[' + nama_bank + '] - [' + akun + '] - [' + no_rek + ']';
  tr_bank += '</td>';
  tr_bank += '</tr>';
  tr_product.after(tr_bank);
 },

 cetak: function (id) {
  window.open(url.base_url(Payroll.module()) + "printFaktur/" + id);
 },

 getDetailInvoice: function (elm) {
  var invoice = $(elm).val();
  $.ajax({
   type: 'POST',
   data: {
    invoice: invoice
   },
   dataType: 'json',
   async: false,
   url: url.base_url(Payroll.module()) + "getDetailInvoice",
   error: function () {
    toastr.error("Gagal");
    message.closeLoading();
   },

   beforeSend: function () {

   },

   success: function (resp) {
    $('input#tanggal_faktur').val(resp.invoice.tanggal_faktur);
    $('input#tanggal_bayar').val(resp.invoice.tanggal_bayar);
    $('div#content-product').html(resp.view_item);
    $('label#total').html(resp.invoice.total);
    $('label#total').attr('total', resp.invoice.total_ori);
    Payroll.hitungSisaBayar(elm);
   }
  });
 },

 hitungSisaBayar: function (elm) {
  var jumlah = $('input#jumlah').val();
  var total = $('label#total').attr('total');
  var sisa_content = $('input#sisa');

  var sisa = jumlah - total;
  sisa_content.val(sisa);
//  $('#sisa').divide({
//   delimiter: '.',
//   divideThousand: true
//  });
 }
};

$(function () {
 Payroll.setDate();
 Payroll.setSelect2();
 Payroll.setThousandSparator();
});