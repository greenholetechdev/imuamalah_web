var Dashboard = {
	randomData: function () {
		barChartData.datasets.forEach(function (dataset) {
			dataset.data = dataset.data.map(function () {
				return randomScalingFactor();
			});
		});
		window.myBar.update();
	},

	getDataPembelian: function () {
		var data_pembelian = $('#data_penjualan').val();
		var pembelian = data_pembelian.toString().split(',');
		var data_all = [];
		for (var i = 0; i < 31; i++) {
			var value = parseFloat(pembelian[i]).toFixed(2);
			// var value = i * 30;
			data_all.push(value);
		}

		//  console.log(data_all);
		return data_all;
	},
  
  getDataKredit: function () {
    var data_pembelian = $('#data_kredit').val();
		var pembelian = data_pembelian.toString().split(',');
		var data_all = [];
		for (var i = 0; i < 12; i++) {
			var value = parseFloat(pembelian[i]).toFixed(2);
			// var value = i * 30;
			data_all.push(value);
		}

		//  console.log(data_all);
		return data_all;
	},

	getDataPembelianResep: function () {
		// var data_pembelian = $('#data_pembelian_resep').val();
		//  var pembelian = data_pembelian.toString().split(',');
		var data_all = [];
		for (var i = 0; i < 31; i++) {
			// var value = parseFloat(pembelian[i]).toFixed(2);
			var value = i * 30;
			data_all.push(value);
		}

		console.log(data_all);
		return data_all;
	},

	getDataLabelPembelian: function () {
		var data = [];

		for (i = 1; i < 32; i++) {
			var value = "" + i;
			data.push(value);
		}


		return data;
	},

  getDataLabelKredit: function () {
		var data = [];

		data.push("Januari");
		data.push("Februari");
		data.push("Maret");
		data.push("April");
		data.push("Mei");
		data.push("Juni");
		data.push("Juli");
		data.push("Agustus");
		data.push("September");
		data.push("Oktober");
		data.push("November");
		data.push("Desember");


		return data;
	},

	setGrafikPembelian: function () {
		var ctx = document.getElementById('canvas_pembelian').getContext('2d');
		var barChartData = {
			labels: Dashboard.getDataLabelPembelian(),
			datasets: [{
				label: 'Rumah',
				backgroundColor: window.chartColors.yellow,
				yAxisID: 'rumah',
				data: Dashboard.getDataPembelian()
			}]
		};

		window.myBar = new Chart(ctx, {
			type: 'bar',
			data: barChartData,
			options: {
				responsive: true,
				title: {
					display: true,
					text: 'Grafik Penjualan'
				},
				tooltips: {
					mode: 'index',
					//    intersect: true
				},
				scales: {
					yAxes: [{
						type: 'linear', // only linear but allow scale type registration. This allows extensions to exist solely for log scale for instance
						display: true,
						position: 'left',
						id: 'rumah',
						ticks: {
							stepSize: 100,
							min: 0,
							max: parseInt($('#total_data_penjualan').val())
						}
					}]
				}
			}
		});
  },
  
	setGrafikPembelianKredit: function () {
		var ctx = document.getElementById('canvas_kredit').getContext('2d');
		var barChartData = {
			labels: Dashboard.getDataLabelKredit(),
			datasets: [{
				label: 'Rumah',
				backgroundColor: window.chartColors.red,
				yAxisID: 'rumah',
				data: Dashboard.getDataKredit()
			}]
		};

		window.myBar = new Chart(ctx, {
			type: 'bar',
			data: barChartData,
			options: {
				responsive: true,
				title: {
					display: true,
					text: 'Grafik Kredit'
				},
				tooltips: {
					mode: 'index',
					//    intersect: true
				},
				scales: {
					yAxes: [{
						type: 'linear', // only linear but allow scale type registration. This allows extensions to exist solely for log scale for instance
						display: true,
						position: 'left',
						id: 'rumah',
						ticks: {
							stepSize: 100,
							min: 0,
							max: parseInt($('#total_data_kredit').val())
						}
					}]
				}
			}
		});
	}
};


$(function () {
  Dashboard.setGrafikPembelian();
  Dashboard.setGrafikPembelianKredit();
});
