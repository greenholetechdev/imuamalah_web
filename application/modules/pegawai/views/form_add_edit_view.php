<input type='hidden' name='' id='id' class='form-control' value='<?php echo isset($id) ? $id : '' ?>'/>
<div class="content">
 <div class="animated fadeIn">
  <div class="card">
   <div class="card-header">
    <div class="row">
     <div class="col-md-10">
      <div class="box-card-title middle-left">
       <i class="mdi mdi-clipboard-plus mdi-18px"></i><strong class="card-title"><?php echo isset($title_content) ? $title_content : '' ?></strong>
      </div>
     </div>
     <div class="col-sm-2 text-right"></div>
    </div>
   </div>
   <div class="card-body card-block">   
    <div class='row'>
     <div class='col-md-12'>
      <u>Data Pegawai</u>
     </div>
    </div> 
    <hr/>
    <div class="row">
     <div class='col-md-3'>
      Nama
     </div>
     <div class='col-md-3'>
      <input type='text' name='' id='nama' class='form-control required' 
             value='<?php echo isset($nama) ? $nama : '' ?>' error="Nama"/>
     </div>     
    </div>
    <br/>
    
    <div class="row">
     <div class='col-md-3'>
      Alamat
     </div>
     <div class='col-md-3'>
      <textarea id="alamat" class="form-control required" error="Alamat"><?php echo isset($alamat) ? $alamat : '' ?></textarea>
     </div>     
    </div>
    <br/>
    
    <div class="row">
     <div class='col-md-3'>
      No HP
     </div>
     <div class='col-md-3'>
      <input type='text' name='' id='no_hp' class='form-control required' 
             value='<?php echo isset($no_hp) ? $no_hp : '' ?>' error="No HP"/>
     </div>     
    </div>
    <br/>
    
    <div class="row">
     <div class='col-md-3'>
      Email
     </div>
     <div class='col-md-3'>
      <input type='text' name='' id='email' class='form-control required' 
             value='<?php echo isset($email) ? $email : '' ?>' error="Email"/>
     </div>     
    </div>
    <br/>
    <hr/>
    <div class='row'>
     <div class='col-md-12 text-right'>
      <button id="" class="btn btn-succes-baru" onclick="Pegawai.simpan('<?php echo isset($id) ? $id : '' ?>')">Simpan</button>
      &nbsp;
      <button id="" class="btn btn-baru" onclick="Pegawai.back()">Kembali</button>
     </div>
    </div>
   </div>
  </div>
 </div>
</div>
