<div class="content">
 <div class="animated fadeIn">
  <div class="card">
   <div class="card-header">
    <div class="row">
     <div class="col-md-10">
      <div class="box-card-title middle-left">
       <i class="mdi mdi-clipboard-plus mdi-18px"></i><strong class="card-title"><?php echo isset($title_content) ? $title_content : '' ?></strong>
      </div>
     </div>
     <div class="col-sm-2 text-right"></div>
    </div>
   </div>
   <div class="card-body card-block">   
    <div class='row'>
     <div class='col-md-12'>
      <u>Data Pegawai</u>
     </div>
    </div> 
    <hr/>
    <div class="row">
     <div class='col-md-3'>
      Nama
     </div>
     <div class='col-md-3'>
      <?php echo $nama ?>
     </div>     
    </div>
    <br/>
    <div class="row">
     <div class='col-md-3'>
      No HP
     </div>
     <div class='col-md-3'>
      <?php echo $no_hp ?>
     </div>     
    </div>
    <br/>
    
    <div class="row">
     <div class='col-md-3'>
      Email
     </div>
     <div class='col-md-3'>
      <?php echo $email ?>
     </div>     
    </div>
    <br/>
    <div class="row">
     <div class='col-md-3'>
      Alamat
     </div>
     <div class='col-md-3'>
      <?php echo $alamat ?>
     </div>     
    </div>
    <br/>
    <hr/>
    <div class='row'>
     <div class='col-md-12 text-right'>
      <button id="" class="btn btn-danger-baru" onclick="Pegawai.back()">Kembali</button>
     </div>
    </div>
   </div>
  </div>
 </div>
</div>
