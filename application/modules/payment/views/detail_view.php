<input type='hidden' name='' id='id' class='form-control' value='<?php echo isset($id) ? $id : '' ?>'/>
<div class="content">
 <div class="animated fadeIn">
  <div class="card">
   <div class="card-header">
    <div class="row">
     <div class="col-md-10">
      <div class="box-card-title middle-left">
       <i class="mdi mdi-clipboard-plus mdi-18px"></i><strong class="card-title"><?php echo isset($title_content) ? $title_content : '' ?></strong>
      </div>
     </div>
     <div class="col-sm-2 text-right">      
     </div>
    </div>
   </div>
   <div class="card-body card-block">   
    <div class='row'>
     <div class='col-md-8'>
      <u>Data Faktur</u>
     </div>
    </div> 
    <hr/>
    <div class="row">
     <div class='col-md-3'>
      No Faktur
     </div>
     <div class='col-md-3'>
      <?php echo $invoice_form['no_faktur'] ?>
     </div>     
    </div>
    <br/>

    <div class="row">
     <div class='col-md-3'>
      Pelanggan
     </div>
     <div class='col-md-3'>
      <?php echo $invoice_form['nama_pembeli'] ?>
     </div>     
    </div>
    <br/>

    <div class="row">
     <div class='col-md-3'>
      Tanggal Faktur
     </div>
     <div class='col-md-3'>
      <?php echo $invoice_form['tanggal_faktur'] ?>
     </div>     
    </div>
    <br/>
    <div class="row">
     <div class='col-md-3'>
      Tanggal Bayar
     </div>
     <div class='col-md-3'>
      <?php echo $invoice_form['tanggal_bayar'] ?>
     </div>     
    </div>
    <br/>        
    <hr/>

    <div class="row">
     <div class="col-md-12">
      <u>Data Produk</u>
     </div>
    </div>
    <br/>

    <div class="row">
     <div class="col-md-10">
      <div class="table-responsive">
       <table class="table table-striped table-bordered table-list-draft" id="tb_product">
        <thead>
         <tr>
          <th>Produk</th>
          <th>Pajak</th>
          <th>Metode Bayar</th>
          <th>Jumlah</th>
          <th>Sub Total</th>
         </tr>
        </thead>
        <tbody>
         <?php if (!empty($invoice_item)) { ?>
          <?php foreach ($invoice_item as $value) { ?>
           <tr> 
            <td><?php echo $value['nama_product'] . '-' . $value['satuan'] . '-[' . number_format($value['harga']) . ']' ?></td>
            <td>
             <?php if ($value['persentase'] != '0') { ?>
              <?php echo $value['jenis'] ?>
             <?php } else { ?>
              <?php echo $value['jenis'] ?>
             <?php } ?>
            </td>
            <td><?php echo $value['metode'] ?></td>
            <td><?php echo $value['qty'] ?></td>
            <td><?php echo number_format($value['sub_total']) ?></td>
           </tr>

           <?php if ($value['bank'] != '0' && $value['bank'] != '') { ?>
            <tr>
             <td colspan="7"><?php echo $value['nama_bank'] . '-' . $value['no_rekening'] . '-' . $value['akun'] ?></td>
            </tr>
           <?php } ?>
          <?php } ?>
         <?php } ?>         
        </tbody>
       </table>
      </div>
     </div>
    </div>

    <div class="row">
     <div class="col-md-10 text-right">
      <h4>Total : Rp, <label id="total"><?php echo number_format($invoice_form['total_ori']) ?></label></h4>
     </div>
    </div>
    <br/>


    <div class='row'>
     <div class='col-md-12'>
      <u>Faktur Bayar</u>
     </div>
    </div> 
    <hr/>

    <div class="row">
     <div class='col-md-3'>
      Tanggal Faktur
     </div>
     <div class='col-md-3'>
      <?php echo $tanggal_faktur ?>
     </div>     
    </div>
    <br/>
    <div class="row">
     <div class='col-md-3'>
      Tanggal Bayar
     </div>
     <div class='col-md-3'>
      <?php echo $tanggal_bayar ?>
     </div>     
    </div>
    <br/>   
    <div class="row">
     <div class='col-md-3'>
      Jumlah
     </div>
     <div class='col-md-3'>
      <?php echo 'Rp, '.number_format($jumlah) ?>
     </div>     
    </div>
    <br/>   
    <div class="row">
     <div class='col-md-3'>
      Sisa
     </div>
     <div class='col-md-3'>
      <?php echo 'Rp, '.number_format($sisa) ?>
     </div>     
    </div>
    <br/>   
    <div class='row'>
     <div class='col-md-12 text-right'>
      <button id="" class="btn btn-danger-baru" onclick="Payment.cetak('<?php echo isset($id) ? $id : '' ?>')">Cetak</button>
      &nbsp;
      <button id="" class="btn btn-baru" onclick="Payment.back()">Kembali</button>
     </div>
    </div>
   </div>
  </div>
 </div>
</div>
