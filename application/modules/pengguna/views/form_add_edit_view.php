<input type='hidden' name='' id='id' class='form-control' value='<?php echo isset($id) ? $id : '' ?>'/>
<div class="content">
 <div class="animated fadeIn">
  <div class="card">
   <div class="card-header">
    <div class="row">
     <div class="col-md-10">
      <div class="box-card-title middle-left">
       <i class="mdi mdi-clipboard-plus mdi-18px"></i><strong class="card-title"><?php echo isset($title_content) ? $title_content : '' ?></strong>
      </div>
     </div>
     <div class="col-sm-2 text-right"></div>
    </div>
   </div>
   <div class="card-body card-block">   
    <div class='row'>
     <div class='col-md-12'>
      <u>Data Pengguna</u>
     </div>
    </div> 
    <hr/>
    <div class="row">
     <div class='col-md-3'>
      Hak Akses
     </div>
     <div class='col-md-3'>
      <select id="akses" onchange="Pengguna.checkAksesAktif(this)" class="form-control required" error="Hak Akses">
       <?php if (!empty($list_akses)) { ?>
        <?php foreach ($list_akses as $value) { ?>       
         <?php
         $selected = "";
         if (isset($hak_akses)) {
          $selected = $hak_akses == $value['hak_akses'] ? 'selected' : '';
         }
         ?>
         <option <?php echo $selected ?> value="<?php echo $value['id'] ?>"><?php echo $value['hak_akses'] ?></option>
        <?php } ?>
       <?php } ?>
      </select>
     </div>     
    </div>
    <br/>

    <div class="row">
     <div class='col-md-3'>
      Username
     </div>
     <div class='col-md-3'>
      <input type='text' name='' id='username' class='form-control required' 
             value='<?php echo isset($username) ? $username : '' ?>' error="Username"/>
     </div>     
    </div>
    <br/>

    <div class="row">
     <div class='col-md-3'>
      Password
     </div>
     <div class='col-md-3'>
      <input type='text' name='' id='password' class='form-control required' 
             value='<?php echo isset($password) ? $password : '' ?>' error="Password"/>
     </div>     
    </div>
    <br/>

    <hr/>
    <div class='row'>
     <div class='col-md-12 text-right'>
      <button id="" class="btn btn-succes-baru" onclick="Pengguna.simpan('<?php echo isset($id) ? $id : '' ?>')">Simpan</button>
      &nbsp;
      <button id="" class="btn btn-baru" onclick="Pengguna.back()">Kembali</button>
     </div>
    </div>
   </div>
  </div>
 </div>
</div>
