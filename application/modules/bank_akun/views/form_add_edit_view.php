<input type='hidden' name='' id='id' class='form-control' value='<?php echo isset($id) ? $id : '' ?>'/>
<div class="content">
 <div class="animated fadeIn">
  <div class="card">
   <div class="card-header">
    <div class="row">
     <div class="col-md-10">
      <div class="box-card-title middle-left">
       <i class="mdi mdi-clipboard-plus mdi-18px"></i><strong class="card-title"><?php echo isset($title_content) ? $title_content : '' ?></strong>
      </div>
     </div>
     <div class="col-sm-2 text-right"></div>
    </div>
   </div>
   <div class="card-body card-block">   
    <div class='row'>
     <div class='col-md-12'>
      <u>Data Akun</u>
     </div>
    </div> 
    <hr/>

    <div class="row">
     <div class='col-md-3'>
      Nama Bank
     </div>
     <div class='col-md-3'>
      <input type='text' name='' placeholder="" id='nama_bank' class='form-control required' 
             value='<?php echo isset($nama_bank) ? $nama_bank : '' ?>' error="Nama Bank"/>
     </div>     
    </div>
    <br/>
   
    <div class="row">
     <div class='col-md-3'>
      No Rekening
     </div>
     <div class='col-md-3'>
      <input type='text' name='' placeholder="" id='no_rekening' class='form-control required' 
             value='<?php echo isset($no_rekening) ? $no_rekening : '' ?>' error="No Rekening"/>
     </div>     
    </div>
    <br/>
    <div class="row">
     <div class='col-md-3'>
      Akun
     </div>
     <div class='col-md-3'>
      <input type='text' name='' placeholder="" id='akun' class='form-control required' 
             value='<?php echo isset($akun) ? $akun : '' ?>' error="Akun"/>
     </div>     
    </div>
    <br/>
    <hr/>
    <div class='row'>
     <div class='col-md-12 text-right'>
      <button id="" class="btn btn-succes-baru" onclick="BankAkun.simpan('<?php echo isset($id) ? $id : '' ?>')">Simpan</button>
      &nbsp;
      <button id="" class="btn btn-baru" onclick="BankAkun.back()">Kembali</button>
     </div>
    </div>
   </div>
  </div>
 </div>
</div>
