<input type='hidden' name='' id='id' class='form-control' value='<?php echo isset($id) ? $id : '' ?>'/>
<div class="content">
 <div class="animated fadeIn">
  <div class="card">
   <div class="card-header">
    <div class="row">
     <div class="col-md-10">
      <div class="box-card-title middle-left">
       <i class="mdi mdi-clipboard-plus mdi-18px"></i><strong class="card-title"><?php echo isset($title_content) ? $title_content : '' ?></strong>
      </div>
     </div>
     <div class="col-sm-2 text-right"></div>
    </div>
   </div>
   <div class="card-body card-block">   
    <div class='row'>
     <div class='col-md-12'>
      <u>Data Periode</u>
     </div>
    </div> 
    <hr/>
    <div class="row">
     <div class='col-md-3'>
      Bulan
     </div>
     <div class='col-md-3'>
      <input value="<?php echo date('m') ?>" type='number' name='' min="1" max="12" id='month' class='form-control required' 
             value='<?php echo isset($month) ? $month : '' ?>' error="Bulan"/>
     </div>     
    </div>
    <br/>
    
    <div class="row">
     <div class='col-md-3'>
      Tahun
     </div>
     <div class='col-md-3'>
      <input type='number' value="<?php echo date('Y') ?>" min="2017" max="<?php echo date('Y') ?>" name='' id='year' class='form-control required' 
             value='<?php echo isset($year) ? $year : '' ?>' error="Tahun"/>
     </div>     
    </div>
    <br/>
    <hr/>
    <div class='row'>
     <div class='col-md-12 text-right'>
      <button id="" class="btn btn-succes-baru" onclick="Periode.simpan('<?php echo isset($id) ? $id : '' ?>')">Simpan</button>
      &nbsp;
      <button id="" class="btn btn-baru" onclick="Periode.back()">Kembali</button>
     </div>
    </div>
   </div>
  </div>
 </div>
</div>
