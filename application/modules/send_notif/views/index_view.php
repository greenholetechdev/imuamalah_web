<div class="content">
 <div class="animated fadeIn">
  <div class="card">
   <div class="card-header">
    <div class="row">
     <div class="col-md-10">
      <div class="box-card-title middle-left">
       <i class="mdi mdi-clipboard-plus mdi-18px"></i><strong class="card-title"><?php echo isset($title_content) ? $title_content : '' ?></strong>
      </div>
     </div>
     <div class="col-sm-2 text-right"></div>
    </div>
   </div>
   <div class="card-body card-block">   
    <div class="row">
     <div class="col-md-3">
      &nbsp;
     </div>
     <div class="col-md-9">
      <div class="form-inside-icon icon-pos-right">
       <input type="text" id="keyword" class="form-control" placeholder="Pencarian" onkeyup="SendNotif.search(this, event)">
       <div class="form-icon">
        <i class="fa fa-search"></i>
       </div>
      </div>
     </div>
    </div>
    <br/>
    <div class='row'>
     <div class='col-md-12'>
      <?php if (isset($keyword)) { ?>
       <?php if ($keyword != '') { ?>
        Cari Data : "<b><?php echo $keyword; ?></b>"
       <?php } ?>
      <?php } ?>
     </div>
    </div>
    <br/>
    <div class='row'>
     <div class='col-md-6'>
      <?php if (!empty($jatuh_tempo)) { ?>
       <?php $message = "* Data yang tampil "; ?>
       <?php $message .= '<i><b>' . $jatuh_tempo['jadwal_notif'] . '</b></i> Hari '; ?>
       <?php $message .= $jatuh_tempo['jenis_notif'] == '+' ? '<i><b>Setelah</b></i>' : '<i><b>Sebelum</b></i>' ?> 
       <?php $message .= " Periode Jatuh Tempo" ?>
       <?php echo $message ?>
      <?php } ?>      
     </div>
    </div>    
    <hr/>
    <br/>
    <div class="row">
     <div class="col-md-12">
      <div class='table-responsive'>
       <table class="table table-striped table-bordered table-list-draft">
        <thead>
         <tr>
          <th>No</th>
          <th>No Penjualan</th>
          <th>Total Angsuran</th>
          <th>Tunggakan Angsuran Ke -</th>
          <th>Periode Angsuran</th>
          <th>Tanggal Angsuran Terakhir</th>
          <th>Notifikasi Terkirim</th>
          <th>Action</th>
         </tr>
        </thead>
        <tbody>
         <?php if (!empty($content)) { ?>
          <?php $no = 1; ?>
          <?php foreach ($content as $value) { ?>
           <tr>
            <td><?php echo $no++ ?></td>
            <td><?php echo $value['nomer_penjualan'] ?></td>
            <td><?php echo $value['total_angsuran'] . 'x' ?></td>
            <td><?php echo $value['tunggakan'] . 'x' ?></td>
            <td><?php echo $value['jumlah_angsuran'] . 'x' ?></td>
            <td><?php echo $value['tanggal_angsuran_terakhir'] ?></td>
            <td><?php echo $value['jumlah_notif'] ?></td>
            <td class="text-center">
             <button no_hp="<?php echo $value['no_hp'] ?>" email="<?php echo $value['email'] ?>" id="" class="btn btn-warning-baru font12" 
                     onclick="SendNotif.send(this, '<?php echo $value['id'] ?>')">Kirim Notif</button>
             &nbsp;
            </td>
           </tr>
          <?php } ?>
         <?php } else { ?>
          <tr>
           <td colspan="8" class="text-center">Tidak ada data ditemukan</td>
          </tr>
         <?php } ?>

        </tbody>
       </table>
      </div>
     </div>          
    </div> 
    <div class="row">
     <div class="col-md-12">
      <div class="pagination">
       <?php echo $pagination['links'] ?>
      </div>
     </div>
    </div>       
   </div>
  </div>
 </div>
</div>
