<input type='hidden' name='' id='pembayaran_rumah' class='form-control' value='<?php echo isset($pembayaran_rumah) ? $pembayaran_rumah : '' ?>'/>
<input type='hidden' name='' id='email' class='form-control' value='<?php echo isset($email) ? $email : '' ?>'/>
<input type='hidden' name='' id='no_hp' class='form-control' value='<?php echo isset($no_hp) ? $no_hp : '' ?>'/>
<div class="content">
 <div class="animated fadeIn">
  <div class="card">
   <div class="card-header">
    <div class="row">
     <div class="col-md-10">
      <div class="box-card-title middle-left">
       <i class="mdi mdi-clipboard-plus mdi-18px"></i><strong class="card-title"><?php echo isset($title_content) ? $title_content : '' ?></strong>
      </div>
     </div>
     <div class="col-sm-2 text-right"></div>
    </div>
   </div>
   <div class="card-body card-block">   
    <div class='row'>
     <div class='col-md-12'>
      <u>Masukan Pesan Notifikasi</u>
     </div>
    </div> 
    <hr/>
    <div class="row">
     <div class='col-md-3'>
      Pesan
     </div>
     <div class='col-md-9'>
      <textarea id="pesan" class="form-control required" error="Pesan"></textarea>
     </div>     
    </div>
    <br/>
    <hr/>
    <div class='row'>
     <div class='col-md-12 text-right'>
      <button id="" class="btn btn-succes-baru" onclick="SendNotif.kirimNotif('<?php echo isset($pembayaran_rumah) ? $pembayaran_rumah : '' ?>')">Proses</button>
     </div>
    </div>
   </div>
  </div>
 </div>
