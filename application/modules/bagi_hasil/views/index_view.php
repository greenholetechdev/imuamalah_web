<div class="content">
 <div class="animated fadeIn">
  <div class="card">
   <div class="card-header">
    <div class="row">
     <div class="col-md-10">
      <div class="box-card-title middle-left">
       <i class="mdi mdi-clipboard-plus mdi-18px"></i><strong class="card-title"><?php echo isset($title_content) ? $title_content : '' ?></strong>
      </div>
     </div>
     <div class="col-sm-2 text-right"></div>
    </div>
   </div>
   <div class="card-body card-block">   
    <div class="row">
     <div class="col-md-3">
      <input type='text' name='' id='date' class='form-control' value='<?php echo date('Y-m-d') ?>'/>            
     </div>
     <div class="col-md-3">
      <button id="" class="btn btn-succes-baru" onclick="BagiHasil.search(this)">Proses</button>
     </div>
    </div>
    <br/>
    <br/>
    <div class="row">
     <div class="col-md-12">
      <div class='table-responsive' id="data_detail">
       <table class="table table-striped table-bordered table-list-draft">
        <thead>
         <tr>
          <th>Keterangan</th>
          <th>Presentase</th>
          <th>Laba Bersih</th>
          <th>Bagi Hasil</th>
         </tr>
        </thead>
        <tbody>
         <?php $total = 0; ?>
         <?php foreach ($internal as $value) { ?>
          <tr>
           <td><?php echo $value['keterangan'] ?></td>
           <td><?php echo $value['presentase'] . ' %' ?></td>
           <td><?php echo 'Rp. ' . number_format($hasil_total, 2, ',', '.') ?></td>
           <?php
           $total = (($hasil_total * $value['presentase']) / 100);
           ?>
           <td><?php echo 'Rp. ' . number_format($total, 2, ',', '.') ?></td>
          </tr>
         <?php } ?>
        </tbody>
       </table>
      </div>
     </div>          
    </div>        
   </div>
  </div>
 </div>
</div>
