<table class="table table-striped table-bordered table-list-draft">
 <thead>
  <tr>
   <th>Keterangan</th>
   <th>Presentase</th>
   <th>Laba Bersih</th>
   <th>Bagi Hasil</th>
  </tr>
 </thead>
 <tbody>
  <?php $total = 0; ?>
  <?php foreach ($internal as $value) { ?>
   <tr>
    <td><?php echo $value['keterangan'] ?></td>
    <td><?php echo $value['presentase'] . ' %' ?></td>
    <td><?php echo 'Rp. ' . number_format($hasil_total, 2, ',', '.') ?></td>
    <?php
    $total = (($hasil_total * $value['presentase']) / 100);
    ?>
    <td><?php echo 'Rp. ' . number_format($total, 2, ',', '.') ?></td>
   </tr>
  <?php } ?>
 </tbody>
</table>