<input type='hidden' name='' id='id' class='form-control' value='<?php echo isset($id) ? $id : '' ?>'/>
<div class="content">
 <div class="animated fadeIn">
  <div class="card">
   <div class="card-header">
    <div class="row">
     <div class="col-md-10">
      <div class="box-card-title middle-left">
       <i class="mdi mdi-clipboard-plus mdi-18px"></i><strong class="card-title"><?php echo isset($title_content) ? $title_content : '' ?></strong>
      </div>
     </div>
     <div class="col-sm-2 text-right"></div>
    </div>
   </div>
   <div class="card-body card-block">   
    <div class='row'>
     <div class='col-md-12'>
      <u>Data Internal</u>
     </div>
    </div> 
    <hr/>

    <div class="row">
     <div class='col-md-12'>
      <table class="table table-striped table-bordered table-list-draft">
       <thead>
        <tr>
         <th>Keterangan</th>
         <th>Presentase (%)</th>
         <th>Action</th>
        </tr>
       </thead>
       <tbody>
        <?php if (!empty($content)) { ?>
         <?php foreach ($content as $value) { ?>
          <tr id="<?php echo $value['id'] ?>">
           <td>
            <input type='text' name='' id='keterangan' class='form-control' value='<?php echo $value['keterangan'] ?>'/>
          </td>
          <td>
           <input type='text' name='' id='presentase' class='form-control' value='<?php echo $value['presentase'] ?>'/>
          </td>
           <td class="text-center">
            &nbsp;
           </td>
          </tr>
         <?php } ?>
         <tr id="">
          <td>
           <input type='text' name='' id='keterangan' class='form-control' value=''/>
          </td>
          <td>
           <input type='text' name='' id='presentase' class='form-control' value=''/>
          </td>
          <td class="text-center">
           <i class="mdi mdi-plus mdi-24px hover" onclick="Internal.addRow(this)"></i>
          </td>
         </tr>
        <?php } else { ?>
         <tr>
          <td colspan="6" class="text-center">Tidak ada data ditemukan</td>
         </tr>
        <?php } ?>

       </tbody>
      </table>
     </div>     
    </div>
    <br/>
    <hr/>
    <div class='row'>
     <div class='col-md-12 text-right'>
      <button id="" class="btn btn-succes-baru" onclick="Internal.simpan('<?php echo isset($id) ? $id : '' ?>')">Simpan</button>
      &nbsp;
      <button id="" class="btn btn-baru" onclick="Internal.back()">Kembali</button>
     </div>
    </div>
   </div>
  </div>
 </div>
</div>
