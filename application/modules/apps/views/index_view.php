<div class="content">
 <div class="row">
  <div class="col-sm-6 col-lg-3">
   <div class="card text-white">
    <div class="card-body pb-0">        
     <p class="">Bank</p>
     <br/>
     <p class="">Fitur (Akun Rekening, Transfer, Transaksi, Rekonsiliasi)</p>
    </div>
   </div>
  </div>
  <div class="col-sm-6 col-lg-3">
   <div class="card text-white">
    <div class="card-body pb-0">        
     <p class="">Aset</p>
     <br/>
     <p class="">Fitur (Master Aset, Depresiasi)</p>
     <br/>
    </div>
   </div>
  </div>
  
  <div class="col-sm-6 col-lg-3">
   <div class="card text-white">
    <div class="card-body pb-0">        
     <p class="">Penggajian</p>
     <br/>
     <p class="">Fitur (Penggajian Tetap, Penggajian Borongan)</p>
    </div>
   </div>
  </div>
  
  <div class="col-sm-6 col-lg-3">
   <div class="card text-white">
    <div class="card-body pb-0">        
     <p class="">Inventori</p>
     <br/>
     <p class="">Fitur (Master Gudang, Master Rak, Integrasi Mobile)</p>
    </div>
   </div>
  </div>
 </div> 
 
 <div class="row">
  <div class="col-sm-6 col-lg-3">
   <div class="card text-white">
    <div class="card-body pb-0">        
     <p class="">Absensi</p>
     <br/>
     <br/>
     <br/>
     <p class="">Fitur (Absen, Cuti, Izin)</p>
    </div>
   </div>
  </div>
  <div class="col-sm-6 col-lg-3">
   <div class="card text-white">
    <div class="card-body pb-0">        
     <p class="">Pengadaan</p>
     <br/>
     <p class="">Fitur (Rencana Anggaran Belanja, Auto Email Order, Retur, RAB Mobile)</p>
     <br/>
    </div>
   </div>
  </div>
  
  <div class="col-sm-6 col-lg-3">
   <div class="card text-white">
    <div class="card-body pb-0">        
     <p class="">Zakat</p>
     <br/>
     <br/>
     <br/>
     <p class="">Fitur (Autocalc Zakat, Jizyah)</p>
    </div>
   </div>
  </div>
  
  <div class="col-sm-6 col-lg-3">
   <div class="card text-white">
    <div class="card-body pb-0">        
     <p class="">Laba Splitter</p>
     <br/>
     <br/>
     <p class="">Fitur (Bunga Bank vs Laba Kotor, Profit & Dividen, Autocalc Bunga Bank)</p>
    </div>
   </div>
  </div>
 </div> 
 
 <div class="row">
  <div class="col-sm-6 col-lg-3">
   <div class="card text-white">
    <div class="card-body pb-0">        
     <p class="">Investasi</p>
     <br/>
     <br/>
     <br/>
     <p class="">Fitur (Autocalc Alokasi Dividen, Multi Akad Syirkah Profit)</p>
    </div>
   </div>
  </div>
  
  <div class="col-sm-6 col-lg-3">
   <div class="card text-white">
    <div class="card-body pb-0">        
     <p class="">Human Resource</p>
     <br/>
     <br/>
     <br/>
     <p class="">Fitur (Master Karyawan, Dokumen Karyawan, Simple Kontrak)</p>
    </div>
   </div>
  </div>
  
  <div class="col-sm-6 col-lg-3">
   <div class="card text-white">
    <div class="card-body pb-0">        
     <p class="">Inventaris Dokumen</p>
     <br/>
     <br/>
     <br/>
     <br/>
     <p class="">Fitur (Upload Dokumen, Scrap Dokumen)</p>
    </div>
   </div>
  </div>
 </div> 
</div>
