<input type='hidden' name='' id='id' class='form-control' value='<?php echo isset($id) ? $id : '' ?>'/>
<div class="content">
 <div class="animated fadeIn">
  <div class="card">
   <div class="card-header">
    <div class="row">
     <div class="col-md-10">
      <div class="box-card-title middle-left">
       <i class="mdi mdi-clipboard-plus mdi-18px"></i><strong class="card-title"><?php echo isset($title_content) ? $title_content : '' ?></strong>
      </div>
     </div>
     <div class="col-sm-2 text-right"></div>
    </div>
   </div>
   <div class="card-body card-block">   
    <div class='row'>
     <div class='col-md-12'>
      <u>Data Persyaratan</u>
     </div>
    </div> 
    <hr/>
    <div class="row">
     <div class='col-md-3'>
      Syarat
     </div>
     <div class='col-md-3'>
      <?php echo $syarat ?>
     </div>     
    </div>
    <br/>

    <div class="row">
     <div class='col-md-3'>
      Kategori Akad
     </div>
     <div class='col-md-3'>
      <?php echo $kategori ?>
     </div>     
    </div>
    <br/>

    <div class="row">
     <div class='col-md-3'>
      Jenis Akad
     </div>
     <div class='col-md-3'>
      <?php echo $jenis ?>
     </div>     
    </div>
    <br/>

    <div class="row">
     <div class='col-md-3'>
      Keterangan
     </div>
     <div class='col-md-3'>
      <?php echo $keterangan ?>
     </div>     
    </div>
    <br/>    
    <hr/>

    <div class="row">
     <div class='col-md-3'>
      <b><u>Data Berkas Persyaratan</u></b>
     </div>     
    </div>
    <br/>

    <div class="row">
     <div class='col-md-12'>
      <table class="table table-striped table-bordered" id="list_berkas">
       <thead>
        <tr>
         <th>Jenis Berkas</th>
         <th>Lampirkan Berkas</th>
        </tr>
       </thead>
       <tbody>
        <?php if (!empty($detail)) { ?>
         <?php foreach ($detail as $value) { ?>
          <tr>
           <td>
            <?php echo $value['nama_berkas'] ?>
           </td>
           <td>
            <?php echo $value['status'] == 'Y' ? 'Wajib' : 'Tidak' ?>
           </td>
          </tr>
         <?php } ?>
        <?php } ?>        
       </tbody>
      </table>

     </div>     
    </div>
    <br/>

    <div class='row'>
     <div class='col-md-12 text-right'>
      <button id="" class="btn btn-baru" onclick="Persyaratan.back()">Kembali</button>
     </div>
    </div>
   </div>
  </div>
 </div>
</div>
