<?php

if (!defined('BASEPATH'))
 exit('No direct script access allowed');

class No_generator extends MX_Controller {

 public function __construct() {
  parent::__construct();
 }

 public function digit_count($length, $value) {
  while (strlen($value) < $length)
   $value = '0' . $value;
  return $value;
 }

 public function generateInvoice($frontID,$table) {
  $no_invoice = $frontID . date('y') . strtoupper(date('M'));
  $data = Modules::run('database/get', array(
  'table' => $table,
  'like' => array(
  array('no_invoice', $no_invoice)
  ),
		'orderby'=> 'id desc'
  ));

  $seq = 1;
  if (!empty($data)) {
   $data = $data->row_array();
   $seq = str_replace($no_invoice, '', $data['no_invoice']);   
			$seq = intval($seq) + 1;
  }
		
  $seq = $this->digit_count(3, $seq);
  $no_invoice .= $seq;
  echo $no_invoice;
 }
 
 public function generateNoFaktur() {
  $no_invoice = 'INV' . date('y') . strtoupper(date('M'));
  $data = Modules::run('database/get', array(
  'table' => 'invoice',
  'like' => array(
  array('no_faktur', $no_invoice)
  ),
		'orderby'=> 'id desc'
  ));

  $seq = 1;
  if (!empty($data)) {
   $data = $data->row_array();
   $seq = str_replace($no_invoice, '', $data['no_faktur']);   
			$seq = intval($seq) + 1;
  }
		
  $seq = $this->digit_count(3, $seq);
  $no_invoice .= $seq;
  echo $no_invoice;
 }
 
 public function generateNoFakturBayar() {
  $no_invoice = 'PAY' . date('y') . strtoupper(date('M'));
  $data = Modules::run('database/get', array(
  'table' => 'payment',
  'like' => array(
  array('no_faktur_bayar', $no_invoice)
  ),
		'orderby'=> 'id desc'
  ));

  $seq = 1;
  if (!empty($data)) {
   $data = $data->row_array();
   $seq = str_replace($no_invoice, '', $data['no_faktur_bayar']);   
			$seq = intval($seq) + 1;
  }
		
  $seq = $this->digit_count(3, $seq);
  $no_invoice .= $seq;
  echo $no_invoice;
 }
 
 public function generateNoFakturBayarSebagian() {
  $no_invoice = 'PAYSB' . date('y') . strtoupper(date('M'));
  $data = Modules::run('database/get', array(
  'table' => 'partial_payment',
  'like' => array(
  array('no_faktur_bayar', $no_invoice)
  ),
		'orderby'=> 'id desc'
  ));

  $seq = 1;
  if (!empty($data)) {
   $data = $data->row_array();
   $seq = str_replace($no_invoice, '', $data['no_faktur_bayar']);   
			$seq = intval($seq) + 1;
  }
		
  $seq = $this->digit_count(3, $seq);
  $no_invoice .= $seq;
  echo $no_invoice;
 }
 
 public function generateNoGajiBayar() {
  $no_invoice = 'PAYSLIP' . date('y') . strtoupper(date('M'));
  $data = Modules::run('database/get', array(
  'table' => 'payroll',
  'like' => array(
  array('no_faktur', $no_invoice)
  ),
		'orderby'=> 'id desc'
  ));

  $seq = 1;
  if (!empty($data)) {
   $data = $data->row_array();
   $seq = str_replace($no_invoice, '', $data['no_faktur']);   
			$seq = intval($seq) + 1;
  }
		
  $seq = $this->digit_count(3, $seq);
  $no_invoice .= $seq;
  echo $no_invoice;
 }

}
