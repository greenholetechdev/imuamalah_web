<input type='hidden' name='' id='id' class='form-control' value='<?php echo isset($id) ? $id : '' ?>'/>
<div class="content">
 <div class="animated fadeIn">
  <div class="card">
   <div class="card-header">
    <div class="row">
     <div class="col-md-10">
      <div class="box-card-title middle-left">
       <i class="mdi mdi-clipboard-plus mdi-18px"></i><strong class="card-title"><?php echo isset($title_content) ? $title_content : '' ?></strong>
      </div>
     </div>
     <div class="col-sm-2 text-right"></div>
    </div>
   </div>
   <div class="card-body card-block">   
    <div class='row'>
     <div class='col-md-12'>
      <u>Data Faktur</u>
     </div>
    </div> 
    <hr/>
    <div class="row">
     <div class='col-md-3'>
      Pelanggan
     </div>
     <div class='col-md-3'>
      <select class="form-control required" id="invoice" 
              error="Invoice"
              onchange="PartialPayment.getDetailInvoice(this)">
       <option value="">Pilih Invoice</option>
       <?php if (!empty($list_invoice)) { ?>
        <?php foreach ($list_invoice as $value) { ?>
         <?php $selected = '' ?>
         <?php if (isset($invoice)) { ?>
          <?php $selected = $invoice == $value['id'] ? 'selected' : '' ?>
         <?php } ?>
         <option <?php echo $selected ?> value="<?php echo $value['id'] ?>"><?php echo $value['no_faktur'] ?></option>
        <?php } ?>
       <?php } ?>
      </select>
     </div>     
    </div>
    <br/>

    <div class="row">
     <div class='col-md-3'>
      Tanggal Faktur
     </div>
     <div class='col-md-3'>
      <input type='text' name='' disabled="" id='tanggal_faktur' class='form-control required' 
             value='<?php echo isset($tanggal_faktur) ? $tanggal_faktur : '' ?>' error="Tanggal Faktur"/>
     </div>     
    </div>
    <br/>
    <div class="row">
     <div class='col-md-3'>
      Tanggal Bayar
     </div>
     <div class='col-md-3'>
      <input type='text' name='' disabled="" id='tanggal_bayar' class='form-control required' 
             value='<?php echo isset($tanggal_bayar) ? $tanggal_bayar : '' ?>' error="Tanggal Bayar"/>
     </div>     
    </div>
    <br/>        
    <hr/>

    <div class="row">
     <div class="col-md-12">
      <u>Data Produk</u>
     </div>
    </div>
    <br/>

    <div class="row">
     <div class="col-md-10">
      <div class="table-responsive" id="content-product">
       <table class="table table-striped table-bordered table-list-draft" id="tb_product">
        <thead>
         <tr>
          <th>Produk</th>
          <th>Pajak</th>
          <th>Metode Bayar</th>
          <th>Jumlah</th>
          <th>Sub Total</th>
          <th>Action</th>
         </tr>
        </thead>
        <tbody>
         <tr data_id="">
          <td colspan="7"class="text-center">
           Tidak ada data ditemukan
          </td>
         </tr>
        </tbody>
       </table>
      </div>
     </div>
    </div>

    <div class="row">
     <div class="col-md-10 text-right">
      <h4>Total : Rp, <label id="total" total="0"></label></h4>
     </div>
    </div>
    <br/>
    
    <?php echo $this->load->view('form_faktur_bayar'); ?>
    
    <div class='row'>
     <div class='col-md-12 text-right'>
      <button id="" class="btn btn-succes-baru" onclick="PartialPayment.simpan('<?php echo isset($id) ? $id : '' ?>')">Simpan</button>
      &nbsp;
      <button id="" class="btn btn-baru" onclick="PartialPayment.back()">Kembali</button>
     </div>
    </div>
   </div>
  </div>
 </div>
</div>
