<input type='hidden' name='' id='id' class='form-control' value='<?php echo isset($id) ? $id : '' ?>'/>
<div class="content">
 <div class="animated fadeIn">
  <div class='row'>
   <div class='col-md-12'>
    <u>Data Lain - lain</u>
   </div>
  </div> 
  <hr/>

  <div class="row">
   <div class='col-md-3'>
    Jenis
   </div>
   <div class='col-md-3'>
    <input type='text' name='' id='jenis' class='form-control required' value='' error="Jenis"/>
   </div>     
  </div>
  <br/>

  <div class="row">
   <div class='col-md-3'>
    Keterangan
   </div>
   <div class='col-md-3'>
    <input type='text' name='' id='keterangan' class='form-control required' 
           value='<?php echo isset($keterangan) ? $keterangan : '' ?>' error="Vendor"/>
   </div>     
  </div>
  <br/>

  <div class="row">
   <div class='col-md-3'>
    Jenis Pembayaran
   </div>
   <div class='col-md-3'>
    <select id="jenis_pembayaran" error="Jenis Pembayaran" class="form-control required">
     <?php if (!empty($list_jenis)) { ?>
      <?php foreach ($list_jenis as $v_j) { ?>
       <option value="<?php echo $v_j['id'] ?>"><?php echo $v_j['jenis'] ?></option>
      <?php } ?>
     <?php } ?>
    </select>
   </div>     
  </div>
  <br/> 

  <div class="row">
   <div class='col-md-3'>
    Nominal
   </div>
   <div class='col-md-3'>
    <input type='text' name='' id='pembayaran' class='form-control text-right required' 
           value='<?php echo isset($Pembayaran) ? $Pembayaran : '0' ?>' error="Vendor"/>
   </div>     
  </div>
  <br/>   

  <div class="row">
   <div class='col-md-3'>
    Tanggal Bayar
   </div>
   <div class='col-md-3'>
    <input type='text' name='' id='tgl_bayar' class='form-control required' 
           value='<?php echo isset($tgl_bayar) ? $tgl_bayar : '' ?>' error="Tanggal Bayar"/>
   </div>     
  </div>
  <br/>   

  <div class="row">
   <div class='col-md-3'>
    Berkas
   </div>
   <div class='col-md-3'>
    <input type='file' name='' id='file' class='' 
           value=''/>
   </div>     
  </div>
  <br/>
  <hr/>
  <div class='row'>
   <div class='col-md-12 text-right'>
    <button id="" class="btn btn-succes-baru" onclick="Pembayaran.simpanLain()">Bayar</button>
    &nbsp;
    <button id="" class="btn btn-baru" onclick="Pembayaran.reloadPage()">Batal</button>
   </div>
  </div>
 </div>
</div>

<script>
 $(function () {
  $('#pembayaran').divide({
   delimiter: '.',
   divideThousand: true
  });
 });
</script>