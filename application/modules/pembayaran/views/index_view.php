<div class="content">
 <div class="animated fadeIn">
  <div class="card">
   <div class="card-header">
    <div class="row">
     <div class="col-md-10">
      <div class="box-card-title middle-left">
       <i class="mdi mdi-clipboard-plus mdi-18px"></i><strong class="card-title"><?php echo isset($title_content) ? $title_content : '' ?></strong>
      </div>
     </div>
     <div class="col-sm-2 text-right"></div>
    </div>
   </div>
   <div class="card-body card-block">       
    <div class='row'>
     <div class='col-md-9'>
      <h4><u>Pilih Metode Pembayaran</u></h4>
     </div>
     <div class='col-md-3 text-right'>
      <i class="mdi mdi-chevron-double-right mdi-18px hover"></i>
      Lihat Semua Data
     </div>
    </div>
    <br/>
    <hr/>

    <div class='row'>
     <div class='col-md-6'>
      <select id="metode_bayar" class="form-control">
       <?php if (!empty($list_pembayaran)) { ?>
        <?php foreach ($list_pembayaran as $v_pem) { ?>
         <option value="<?php echo $v_pem['id'] ?>"><?php echo $v_pem['metode'] ?></option>
        <?php } ?>
       <?php } ?>
      </select>
     </div>
     <div class='col-md-6 text-left'>
      <button id="" class="btn btn-succes-baru" onclick="Pembayaran.getPembayaranForm()">Proses</button>
     </div>
    </div>
    <br/>
    
    <div class='form_pembayaran'>
     
    </div>
   </div>
  </div>
 </div>
</div>
