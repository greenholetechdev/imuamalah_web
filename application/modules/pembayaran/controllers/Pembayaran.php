<?php

class Pembayaran extends MX_Controller {

 public $segment;
 public $limit;
 public $page;
 public $last_no;

 public function __construct() {
  parent::__construct();
  $this->limit = 10;
 }

 public function getModuleName() {
  return 'pembayaran';
 }

 public function getHeaderJSandCSS() {
  $data = array(
  '<script src="' . base_url() . 'assets/js/bootbox.js"></script>',
  '<script src="' . base_url() . 'assets/js/controllers/pembayaran.js"></script>'
  );

  return $data;
 }

 public function getTableName() {
  return 'pembayaran_product';
 }

 public function index() {
  $this->segment = 3;
  $this->page = $this->uri->segment($this->segment) ?
  $this->uri->segment($this->segment) - 1 : 0;
  $this->last_no = $this->page * $this->limit;

  $data['view_file'] = 'index_view';
  $data['header_data'] = $this->getHeaderJSandCSS();
  $data['module'] = $this->getModuleName();
  $data['title'] = "Data Pembayaran";
  $data['title_content'] = 'Data Pembayaran';
  $content = $this->getDataPembayaran();
  $data['content'] = $content['data'];
  $total_rows = $content['total_rows'];
  $data['list_pembayaran'] = $this->getMetodePembayaran();
  $data['pagination'] = Modules::run('pagination/get_pagination', $this->getModuleName() . '/index/', $this->segment, $total_rows, $this->limit, $this->last_no);
  echo Modules::run('template', $data);
 }

 public function getMetodePembayaran() {
  $data = Modules::run('database/get', array(
  'table' => 'metode_pembayaran',
  ));

  $result = array();
  if (!empty($data)) {
   foreach ($data->result_array() as $value) {
    array_push($result, $value);
   }
  }


  return $result;
 }

 public function getListTagihan() {
  $data = Modules::run('database/get', array(
  'table' => 'tagihan',
  'where'=> "deleted = 0 or deleted is null"
  ));

  $result = array();
  if (!empty($data)) {
   foreach ($data->result_array() as $value) {
    array_push($result, $value);
   }
  }


  return $result;
 }

 public function getListVendor() {
  $data = Modules::run('database/get', array(
  'table' => 'vendor',
  'where' => "(deleted = 0 or deleted is null) and status = 1"
  ));

  $result = array();
  if (!empty($data)) {
   foreach ($data->result_array() as $value) {
    array_push($result, $value);
   }
  }


  return $result;
 }

 public function getListJenisPembayaran() {
  $data = Modules::run('database/get', array(
  'table' => 'jenis_pembayaran',
  ));

  $result = array();
  if (!empty($data)) {
   foreach ($data->result_array() as $value) {
    array_push($result, $value);
   }
  }


  return $result;
 }

 public function formPembayaran() {
  $metode = $this->input->post('metode');
  $data = array();
  $view = "";
  switch ($metode) {
   case 1:
    $data['list_tagihan'] = $this->getListTagihan();
    $data['list_jenis'] = $this->getListJenisPembayaran();
    $view = $this->load->view('form_tagihan', $data, true);
    break;

   case 2:
    $data['list_vendor'] = $this->getListVendor();
    $data['list_jenis'] = $this->getListJenisPembayaran();
    $view = $this->load->view('form_vendor', $data, true);
    break;
   case 3:
    $data['list_jenis'] = $this->getListJenisPembayaran();
    $view = $this->load->view('form_lain', $data, true);
    break;
   default:

    break;
  }

  echo $view;
 }

 public function getTotalDataPembayaran($keyword = '') {
  $like = array();
  if ($keyword != '') {
   $like = array(
   array('p.no_invoice', $keyword)
   );
  }
  $total = Modules::run('database/count_all', array(
  'table' => $this->getTableName() . ' p',
  'field' => array('p.*'),
  'like' => $like,
  'is_or_like' => true,
  ));

  return $total;
 }

 public function getDataPembayaran($keyword = '') {
  $like = array();
  if ($keyword != '') {
   $like = array(
   array('p.no_invoice', $keyword)
   );
  }
  $data = Modules::run('database/get', array(
  'table' => $this->getTableName() . ' p',
  'field' => array('p.*'),
  'like' => $like,
  'is_or_like' => true,
  'limit' => $this->limit,
  'offset' => $this->last_no,
  ));

  $result = array();
  if (!empty($data)) {
   foreach ($data->result_array() as $value) {
    array_push($result, $value);
   }
  }

  return array(
  'data' => $result,
  'total_rows' => $this->getTotalDataPembayaran($keyword)
  );
 }

 public function getDetailDataPembayaran($id) {
  $data = Modules::run('database/get', array(
  'table' => $this->getTableName() . ' t',
  'where' => "t.id = '" . $id . "'"
  ));

  return $data->row_array();
 }

 public function getDetailDataPembayaranTagihan($id) {
  $data = Modules::run('database/get', array(
  'table' => 'pembayaran_tagihan' . ' t',
  'field' => array('t.*', 'tg.tagihan as jenis_tagihan', 'jp.jenis'),
  'join' => array(
  array('tagihan tg', 't.tagihan = tg.id'),
  array('jenis_pembayaran jp', 't.jenis_pembayaran = jp.id'),
  ),
  'where' => "t.id = '" . $id . "'"
  ));

  return $data->row_array();
 }

 public function getDetailDataPembayaranLain($id) {
  $data = Modules::run('database/get', array(
  'table' => 'pembayaran_lain_lain' . ' t',
  'field' => array('t.*', 'jp.jenis as jenis_bayar'),
  'join' => array(
  array('jenis_pembayaran jp', 't.jenis_pembayaran = jp.id'),
  ),
  'where' => "t.id = '" . $id . "'"
  ));

  return $data->row_array();
 }

 public function getDetailDataPembayaranVendor($id) {
  $data = Modules::run('database/get', array(
  'table' => 'pembayaran_vendor' . ' t',
  'field' => array('t.*', 'v.nama_vendor', 'jp.jenis'),
  'join' => array(
  array('vendor v', 't.vendor = v.id'),
  array('jenis_pembayaran jp', 't.jenis_pembayaran = jp.id'),
  ),
  'where' => "t.id = '" . $id . "'"
  ));

  return $data->row_array();
 }

 public function add() {
  $data['view_file'] = 'form_add_edit_view';
  $data['header_data'] = $this->getHeaderJSandCSS();
  $data['module'] = $this->getModuleName();
  $data['title'] = "Tambah Pembayaran";
  $data['title_content'] = 'Tambah Pembayaran';
  echo Modules::run('template', $data);
 }

 public function ubah($id) {
  $data = $this->getDetailDataPembayaran($id);
  $data['view_file'] = 'form_add_edit_view';
  $data['header_data'] = $this->getHeaderJSandCSS();
  $data['module'] = $this->getModuleName();
  $data['title'] = "Ubah Pembayaran";
  $data['title_content'] = 'Ubah Pembayaran';
  echo Modules::run('template', $data);
 }

 public function detail($id) {
  $data = $this->getDetailDataPembayaranLain($id);
  $data['view_file'] = 'detail_view';
  $data['header_data'] = $this->getHeaderJSandCSS();
  $data['module'] = $this->getModuleName();
  $data['title'] = "Detail Pembayaran";
  $data['title_content'] = 'Detail Pembayaran';
  echo Modules::run('template', $data);
 }

 public function detailTagihan($id) {
  $data = $this->getDetailDataPembayaranTagihan($id);
  $data['view_file'] = 'detail_tagihan';
  $data['header_data'] = $this->getHeaderJSandCSS();
  $data['module'] = $this->getModuleName();
  $data['title'] = "Detail Pembayaran";
  $data['title_content'] = 'Detail Pembayaran';
  echo Modules::run('template', $data);
 }

 public function detailVendor($id) {
  $data = $this->getDetailDataPembayaranVendor($id);
  $data['view_file'] = 'detail_vendor';
  $data['header_data'] = $this->getHeaderJSandCSS();
  $data['module'] = $this->getModuleName();
  $data['title'] = "Detail Pembayaran";
  $data['title_content'] = 'Detail Pembayaran';
  echo Modules::run('template', $data);
 }

 public function getPostDataHeader($value) {
  $no_invoice = Modules::run('no_generator/generateInvoice', 'INVPEMLAIN', 'pembayaran_lain_lain');
  $data['no_invoice'] = $no_invoice;
  $data['jenis'] = $value->jenis;
  $data['keterangan'] = $value->keterangan;
  $data['total'] = str_replace('.', '', $value->pembayaran);
  $data['jenis_pembayaran'] = $value->jenis_pembayaran;
  $data['tgl_bayar'] = $value->tgl_bayar;
  return $data;
 }

 public function simpanLain() {
  $data = json_decode($this->input->post('data'));
//  echo '<pre>';
//  print_r($data);
//  die;
  $id = $this->input->post('id');
  $is_valid = false;
  $lain = $id;
  $this->db->trans_begin();
  try {
   $post_lain = $this->getPostDataHeader($data);
   if ($id == '') {
    $post_lain['file'] = $_FILES['file']['name'];
    $lain = Modules::run('database/_insert', 'pembayaran_lain_lain', $post_lain);
    $this->uploadData('file');
   }
   $this->db->trans_commit();
   $is_valid = true;
  } catch (Exception $ex) {
   $this->db->trans_rollback();
  }

  echo json_encode(array('is_valid' => $is_valid, 'lain' => $lain));
 }

 public function getPostDataBayarTagihan($value) {
  $no_invoice = Modules::run('no_generator/generateInvoice', 'INVPEMTAG', 'pembayaran_tagihan');
  $data['no_invoice'] = $no_invoice;
  $data['tagihan'] = $value->tagihan;
  $data['keterangan'] = $value->keterangan;
  $data['total'] = str_replace('.', '', $value->pembayaran);
  $data['jenis_pembayaran'] = $value->jenis_pembayaran;
  $data['tgl_bayar'] = $value->tgl_bayar;
  return $data;
 }

 public function getPostDataBayarVendor($value) {
  $no_invoice = Modules::run('no_generator/generateInvoice', 'INVPEMVEN', 'pembayaran_vendor');
  $data['no_invoice'] = $no_invoice;
  $data['vendor'] = $value->vendor;
  $data['keterangan'] = $value->keterangan;
  $data['total'] = str_replace('.', '', $value->pembayaran);
  $data['jenis_pembayaran'] = $value->jenis_pembayaran;
  $data['tgl_bayar'] = $value->tgl_bayar;
  return $data;
 }

 public function simpanTagihan() {
  $data = json_decode($this->input->post('data'));
  $id = $this->input->post('id');
  $is_valid = false;
  $tagihan = $id;
  $this->db->trans_begin();
  try {
   $post_tagihan = $this->getPostDataBayarTagihan($data);
   if ($id == '') {
    $post_tagihan['file'] = $_FILES['file']['name'];
    $tagihan = Modules::run('database/_insert', 'pembayaran_tagihan', $post_tagihan);
    $this->uploadData("file");
   }
   $this->db->trans_commit();
   $is_valid = true;
  } catch (Exception $ex) {
   $this->db->trans_rollback();
  }

  echo json_encode(array('is_valid' => $is_valid, 'tagihan' => $tagihan));
 }

 public function simpanVendor() {
  $data = json_decode($this->input->post('data'));
  $is_valid = false;
  $vendor = '';
  $this->db->trans_begin();
  try {
   $post_vendor = $this->getPostDataBayarVendor($data);
   $post_vendor['file'] = $_FILES['file']['name'];
   $vendor = Modules::run('database/_insert', 'pembayaran_vendor', $post_vendor);
   $this->uploadData('file');
   $this->db->trans_commit();
   $is_valid = true;
  } catch (Exception $ex) {
   $this->db->trans_rollback();
  }

  echo json_encode(array('is_valid' => $is_valid, 'vendor' => $vendor));
 }

 public function search($keyword) {
  $this->segment = 4;
  $this->page = $this->uri->segment($this->segment) ?
  $this->uri->segment($this->segment) - 1 : 0;
  $this->last_no = $this->page * $this->limit;
  $keyword = urldecode($keyword);

  $data['keyword'] = $keyword;
  $data['view_file'] = 'index_view';
  $data['header_data'] = $this->getHeaderJSandCSS();
  $data['module'] = $this->getModuleName();
  $data['title'] = "Data Pembayaran";
  $data['title_content'] = 'Data Pembayaran';
  $content = $this->getDataPembayaran($keyword);
  $data['content'] = $content['data'];
  $total_rows = $content['total_rows'];
  $data['pagination'] = Modules::run('pagination/get_pagination', $this->getModuleName() . '/index/', $this->segment, $total_rows, $this->limit, $this->last_no);
  echo Modules::run('template', $data);
 }

 public function uploadData($name_of_field) {
  $config['upload_path'] = 'files/berkas/pembayaran/';
  $config['allowed_types'] = 'gif|jpg|png|pdf';
  $config['max_size'] = '1000';
  $config['max_width'] = '2000';
  $config['max_height'] = '2000';

  $this->load->library('upload', $config);
  $this->upload->do_upload($name_of_field);
 }

}
