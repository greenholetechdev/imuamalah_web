<input type='hidden' name='' id='id' class='form-control' value='<?php echo isset($id) ? $id : '' ?>'/>
<div class="content">
 <div class="animated fadeIn">
  <div class="card">
   <div class="card-header">
    <div class="row">
     <div class="col-md-10">
      <div class="box-card-title middle-left">
       <i class="mdi mdi-clipboard-plus mdi-18px"></i><strong class="card-title"><?php echo isset($title_content) ? $title_content : '' ?></strong>
      </div>
     </div>
     <div class="col-sm-2 text-right"></div>
    </div>
   </div>
   <div class="card-body card-block">   
    <div class='row'>
     <div class='col-md-12'>
      <u>Data Stok</u>
     </div>
    </div> 
    <hr/>
    <div class="row">
     <div class='col-md-3'>
      Produk
     </div>
     <div class='col-md-3'>
      <select class="form-control required" id="product" 
              error="Produk" onchange="ProductStock.getSatuanProduk(this)">
       <option value="">Pilih Produk</option>
       <?php if (!empty($list_product)) { ?>
        <?php foreach ($list_product as $value) { ?>
         <?php $selected = '' ?>
         <?php if (isset($product)) { ?>
          <?php $selected = $product == $value['id'] ? 'selected' : '' ?>
         <?php } ?>
         <option <?php echo $selected ?> value="<?php echo $value['id'] ?>"><?php echo $value['product'] ?></option>
        <?php } ?>
       <?php } ?>
      </select>
     </div>     
    </div>
    <br/>

    <div class="row">
     <div class='col-md-3'>
      Satuan
     </div>
     <div class='col-md-3' id="content-satuan">
      <select class="form-control required" id="product_satuan" 
              error="Satuan">
       <option value="">Pilih Satuan</option>
       <?php if (isset($list_satuan)) { ?>
        <?php if (!empty($list_satuan)) { ?>
         <?php foreach ($list_satuan as $value) { ?>
          <?php $selected = '' ?>
          <?php if (isset($product_satuan)) { ?>
           <?php $selected = $product_satuan == $value['id'] ? 'selected' : '' ?>
          <?php } ?>
          <option <?php echo $selected ?> value="<?php echo $value['id'] ?>"><?php echo $value['satuan'] ?></option>
         <?php } ?>
        <?php } ?>
       <?php } ?>       
      </select>
     </div>     
    </div>
    <br/>

    <div class="row">
     <div class='col-md-3'>
      Stok
     </div>
     <div class='col-md-3'>
      <input type='text' name='' id='stock' class='form-control text-right required' 
             value='<?php echo isset($stock) ? $stock : '0' ?>' error="Stok"/>
     </div>     
    </div>
    <br/>
    <hr/>
    <div class='row'>
     <div class='col-md-12 text-right'>
      <button id="" class="btn btn-succes-baru" onclick="ProductStock.simpan('<?php echo isset($id) ? $id : '' ?>')">Simpan</button>
      &nbsp;
      <button id="" class="btn btn-baru" onclick="ProductStock.back()">Kembali</button>
     </div>
    </div>
   </div>
  </div>
 </div>
</div>
