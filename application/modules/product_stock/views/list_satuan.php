<select class="form-control required" id="product_satuan" 
        error="Satuan">
 <option value="">Pilih Satuan</option>
 <?php if (!empty($list_satuan)) { ?>
  <?php foreach ($list_satuan as $value) { ?>
   <?php $selected = '' ?>
   <option <?php echo $selected ?> value="<?php echo $value['id'] ?>"><?php echo $value['satuan'] ?></option>
  <?php } ?>
 <?php } ?>
</select>

<script>
 $(function(){
  $('select#product_satuan').select2();
 });
</script>