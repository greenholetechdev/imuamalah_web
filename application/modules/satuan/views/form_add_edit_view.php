<input type='hidden' name='' id='id' class='form-control' value='<?php echo isset($id) ? $id : '' ?>'/>
<div class="content">
 <div class="animated fadeIn">
  <div class="card">
   <div class="card-header">
    <div class="row">
     <div class="col-md-10">
      <div class="box-card-title middle-left">
       <i class="mdi mdi-clipboard-plus mdi-18px"></i><strong class="card-title"><?php echo isset($title_content) ? $title_content : '' ?></strong>
      </div>
     </div>
     <div class="col-sm-2 text-right"></div>
    </div>
   </div>
   <div class="card-body card-block">   
    <div class='row'>
     <div class='col-md-12'>
      <u>Data Stok</u>
     </div>
    </div> 
    <hr/>
    <div class="row">
     <div class='col-md-3'>
      Produk
     </div>
     <div class='col-md-3'>
      <select class="form-control required" id="product" error="Produk">
       <option value="">Pilih Produk</option>
       <?php if (!empty($list_product)) { ?>
        <?php foreach ($list_product as $value) { ?>
         <?php $selected = '' ?>
         <?php if (isset($product)) { ?>
          <?php $selected = $product == $value['id'] ? 'selected' : '' ?>
         <?php } ?>
         <option <?php echo $selected ?> value="<?php echo $value['id'] ?>"><?php echo $value['product'] ?></option>
        <?php } ?>
       <?php } ?>
      </select>
     </div>     
    </div>
    <br/>

    <div class="row">
     <div class='col-md-3'>
      Satuan
     </div>
     <div class='col-md-3'>
      <input type='text' name='' placeholder="Dus/Pack" id='satuan' class='form-control required' 
             value='<?php echo isset($satuan) ? $satuan : '' ?>' error="Satuan"/>
     </div>     
    </div>
    <br/>
   
    <div class="row">
     <div class='col-md-3'>
      Harga
     </div>
     <div class='col-md-3'>
      <input type='text' name=''  placeholder="" id='harga' 
             class='form-control required text-right' 
             value='<?php echo isset($harga) ? $harga : '' ?>' error="Harga"/>
     </div>     
    </div>
    <br/>
    <div class="row">
     <div class='col-md-3'>
      Level
     </div>
     <div class='col-md-3'>
      <input type='number' name='' min="1" placeholder="1" id='level' class='form-control required' 
             value='<?php echo isset($level) ? $level : '' ?>' error="Level"/>
     </div>     
    </div>
    <br/>
    <hr/>
    <div class='row'>
     <div class='col-md-12 text-right'>
      <button id="" class="btn btn-succes-baru" onclick="Satuan.simpan('<?php echo isset($id) ? $id : '' ?>')">Simpan</button>
      &nbsp;
      <button id="" class="btn btn-baru" onclick="Satuan.back()">Kembali</button>
     </div>
    </div>
   </div>
  </div>
 </div>
</div>
