<?php

class Template extends MX_Controller {

 public function index($data) { 
  $data['general_title'] = $this->getDataGeneralTitle();
  echo $this->load->view('template_view', $data, true);
 }

 public function getDataGeneralTitle() {
  $data = Modules::run('database/get', array(
  'table' => 'general',
  ));
  
  $title = "";
  if (!empty($data)) {
   $title = $data->row_array()['title'];
  }


  return $title;
 }
}
