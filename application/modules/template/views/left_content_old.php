<aside id="left-panel" class="left-panel">
 <nav class="navbar navbar-expand-sm navbar-default">

  <div class="navbar-header">
   <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#main-menu" aria-controls="main-menu" aria-expanded="false" aria-label="Toggle navigation">
    <i class="fa fa-bars"></i>
   </button>
   <a class="navbar-brand" href="javascript:;">IMuamalah</a>
   <a class="navbar-brand hidden">&nbsp;</a>
   <a id="openToggle" class="menutoggle pull-left"><i style="margin-top: 12px;" class="fa fa-tasks"></i></a>
  </div>

  <div id="main-menu" class="main-menu collapse navbar-collapse">
   <ul class="nav navbar-nav">
    <li>
     <a href="<?php echo base_url() . 'dashboard/dashboard' ?>"> <i class="menu-icon mdi mdi-file-chart mdi-18px"></i>Dashboard </a>
    </li>        
    <li class="menu-item-has-children dropdown">
     <a href="#" class="dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
      <i class="menu-icon mdi mdi-chevron-double-down mdi-18px"></i>Pemasukan
     </a>
     <ul class="sub-menu children dropdown-menu">
      <li style="padding-left: 16px;" class="pemasukan">
       <a href="<?php echo base_url() . 'kas' ?>"> Tambah Kas </a>
      </li>
      <li style="padding-left: 16px;" class="pemasukan">
       <a href="<?php echo base_url() . 'faktur' ?>"> Faktur </a>
      </li>
      <li style="padding-left: 16px;" class="pemasukan">
       <a href="<?php echo base_url() . 'investor' ?>"> Investor </a>
      </li>
     </ul>    
    </li>    

    <li class="menu-item-has-children dropdown">
     <a href="#" class="dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
      <i class="menu-icon mdi mdi-chevron-double-up mdi-18px"></i>Pengeluaran
     </a>
     <ul class="sub-menu children dropdown-menu">
      <li style="padding-left: 16px;" class="pengeluaran">
       <a href="<?php echo base_url() . 'tagihan' ?>"> Tagihan </a>
      </li>
      <li style="padding-left: 16px;" class="pengeluaran">
       <a href="<?php echo base_url() . 'pembayaran' ?>"> Pembayaran </a>
      </li>
      <li style="padding-left: 16px;" class="pengeluaran">
       <a href="<?php echo base_url() . 'vendor' ?>"> Vendor </a>
      </li>
     </ul>    
    </li>               

    <li class="menu-item-has-children dropdown">
     <a href="#" class="dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
      <i class="menu-icon mdi mdi-account-multiple mdi-18px"></i>Pelanggan
     </a>
     <ul class="sub-menu children dropdown-menu">
      <li style="padding-left: 16px;" class="pelanggan">
       <a href="<?php echo base_url() . 'pelanggan/add' ?>"> Tambah Pelanggan </a>
      </li>
      <li style="padding-left: 16px;" class="pengeluaran">
       <a href="<?php echo base_url() . 'pelanggan' ?>"> Tabel Pelanggan </a>
      </li>
     </ul>    
    </li>            

    <li class="menu-item-has-children dropdown">
     <a href="#" class="dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
      <i class="menu-icon mdi mdi-chevron-double-right mdi-18px"></i>Penjualan Bersyarat
     </a>
     <ul class="sub-menu children dropdown-menu">
      <li style="padding-left: 16px;" class="penjualan">
       <a href="<?php echo base_url() . 'pembelian_produk/add' ?>"> Tambah Penjualan </a>
      </li>
      <li style="padding-left: 16px;" class="penjualan">
       <a href="<?php echo base_url() . 'tenor_angsuran' ?>"> Tenor Angsuran </a>
      </li>
      <li style="padding-left: 16px;" class="penjualan">
       <a href="<?php echo base_url() . 'pembelian_produk' ?>"> Tabel Penjualan </a>
      </li>
      <li style="padding-left: 16px;" class="penjualan">
       <a href="<?php echo base_url() . 'notif_jatuh_tempo' ?>"> Notifikasi Jatuh Tempo </a>
      </li>
      <li style="padding-left: 16px;" class="penjualan">
       <a href="<?php echo base_url() . 'send_notif' ?>"> Kirim Notifikasi </a>
      </li>
      <li style="padding-left: 16px;" class="penjualan">
       <a href="<?php echo base_url() . 'persyaratan' ?>"> Tabel Persyaratan </a>
      </li>
      <li style="padding-left: 16px;" class="penjualan">
       <a href="<?php echo base_url() . 'persyaratan/add' ?>"> Tambah Persyaratan </a>
      </li>
     </ul>    
    </li> 

    <li class="menu-item-has-children dropdown">
     <a href="#" class="dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
      <i class="menu-icon mdi mdi-home-variant mdi-18px"></i>Produk
     </a>
     <ul class="sub-menu children dropdown-menu">
      <li style="padding-left: 16px;" class="properti">
       <a href="<?php echo base_url() . 'kategori_produk' ?>"> Kategori Produk </a>
      </li>
      <li style="padding-left: 16px;" class="properti">
       <a href="<?php echo base_url() . 'tipe_produk' ?>"> Tipe Produk </a>
      </li>    
      <li style="padding-left: 16px;" class="properti">
       <a href="<?php echo base_url() . 'produk' ?>"> Tabel Produk </a>
      </li>
      <li style="padding-left: 16px;" class="properti">
       <a href="<?php echo base_url() . 'satuan' ?>"> Satuan Produk </a>
      </li>
     </ul>    
    </li>    

    <li class="menu-item-has-children dropdown">
     <a href="#" class="dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
      <i class="menu-icon mdi mdi-bank mdi-18px"></i>Bank
     </a>
     <ul class="sub-menu children dropdown-menu">
      <li style="padding-left: 1px;" class="bank">
       <span>
        <a href="<?php echo base_url() . 'bank_akun' ?>"> Akun Rekening</a>
       </span>
      </li>
      <li style="padding-left: 1px;" class="bank">
       <span>        
        <a href="<?php echo base_url() . '' ?>" onclick="Template.showUpdateSystem(this, event)"> Transfer </a>
        <label class="text-right" style="margin-left: 32px;padding: 3px;background: yellow;color:black;border-radius: 3px;font-size: 12px;">PRO</label>
       </span>
      </li>    
      <li style="padding-left: 1px;" class="bank">
       <span>
        <a href="<?php echo base_url() . '' ?>" onclick="Template.showUpdateSystem(this, event)"> Transaksi </a>        
        <label class="text-right" style="margin-left: 32px;padding: 3px;background: yellow;color:black;border-radius: 3px;font-size: 12px;">PRO</label>
       </span>
      </li>
     </ul>    
    </li>    

    <li class="menu-item-has-children dropdown">
     <a href="#" class="dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
      <i class="menu-icon mdi mdi-file-document mdi-18px"></i>Laporan
     </a>
     <ul class="sub-menu children dropdown-menu">
      <li style="padding-left: 1px;" class="laporan">
       <a href="<?php echo base_url() . 'laba_rugi' ?>"> Laba Rugi </a>
      </li>
      <li style="padding-left: 1px;" class="properti">
       <a href="<?php echo base_url() . 'bagi_hasil' ?>"> Bagi Hasil </a>
      </li>    
<!--      <li style="padding-left: 1px;" class="properti">
       <a href="<?php echo base_url() . 'laporan' ?>"> Laporan Keuangan </a>
      </li>    -->
      <li style="padding-left: 1px;" class="properti">
       <span>
        <a href="" onclick="Template.showUpdateSystem(this, event)"> Ringkasan Pemasukan </a>
        <label class="text-right" style="padding: 3px;background: yellow;color:black;border-radius: 3px;font-size: 12px;">PRO</label>
       </span>       
      </li>    
      <li style="padding-left: 1px;" class="properti">
       <span>
        <a href="" onclick="Template.showUpdateSystem(this, event)"> Ringkasan Pengeluaran </a>
        <label class="text-right" style="padding: 3px;background: yellow;color:black;border-radius: 3px;font-size: 12px;">PRO</label>
       </span>
      </li>    
      <li style="padding-left: 1px;" class="properti">
       <span>        
        <a href="" onclick="Template.showUpdateSystem(this, event)"> Pemasukan vs Pengeluaran </a>
        <label class="text-right" style="padding: 3px;background: yellow;color:black;border-radius: 3px;font-size: 12px;">PRO</label>
       </span>
      </li>    
      <li style="padding-left: 1px;" class="properti">
       <span>
        <a href="" onclick="Template.showUpdateSystem(this, event)"> Ringkasan Pajak </a>
        <label class="text-right" style="padding: 3px;background: yellow;color:black;border-radius: 3px;font-size: 12px;">PRO</label>
       </span>
      </li>    
      <li style="padding-left: 1px;" class="properti">
       <span>        
        <a href="" onclick="Template.showUpdateSystem(this, event)"> Ringkasan Arus Kas </a>
        <label class="text-right" style="padding: 3px;background: yellow;color:black;border-radius: 3px;font-size: 12px;">PRO</label>
       </span>
      </li>    
     </ul>    
    </li>    

    <li class="menu-item-has-children dropdown">
     <a href="#" class="dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
      <i class="menu-icon mdi mdi-account-multiple-plus mdi-18px"></i>Kerjasama
     </a>
     <ul class="sub-menu children dropdown-menu">
      <li style="padding-left: 16px;" class="kerja_sama">
       <a href="<?php echo base_url() . 'kerja_sama_internal' ?>"> Internal </a>
      </li>
      <li style="padding-left: 16px;" class="kerja_sama disabled">
       <a href="" onclick="Template.showUpdateSystem(this, event)" > Eksternal </a>
      </li>
     </ul>    
    </li>    

    <li class="menu-item-has-children dropdown">
     <a href="#" class="dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
      <i class="menu-icon mdi mdi-account-check mdi-18px"></i>Akad
     </a>
     <ul class="sub-menu children dropdown-menu">
      <li style="padding-left: 16px;" class="akad">
       <a href="<?php echo base_url() . 'kategori_akad' ?>"> Kategori Akad </a>
      </li>
      <li style="padding-left: 16px;" class="akad">
       <a href="<?php echo base_url() . 'jenis_akad' ?>"> Jenis Akad </a>
      </li>
     </ul>    
    </li>    
    
    <li class="menu-item-has-children dropdown">
     <a href="#" class="dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
      <i class="menu-icon mdi mdi-file-document mdi-18px"></i>Inventori
     </a>
     <ul class="sub-menu children dropdown-menu">
      <li style="padding-left: 16px;" class="akad">
       <a href="<?php echo base_url() . 'product_stock' ?>"> Produk Stok</a>
      </li>
     </ul>    
    </li>    

    <li class="menu-item-has-children dropdown">
     <a href="#" class="dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
      <i class="menu-icon mdi mdi-file-document mdi-18px"></i>Akuntansi
     </a>
     <ul class="sub-menu children dropdown-menu">
      <li style="padding-left: 16px;" class="akad">
       <a href="<?php echo base_url() . 'faktur_pelanggan' ?>"> Faktur Pelanggan</a>
       <a href="<?php echo base_url() . 'pajak' ?>"> Pajak</a>
      </li>
     </ul>    
    </li>    
   
    <li class="menu-item-has-children dropdown">
     <a href="#" class="dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
      <i class="menu-icon mdi mdi-file-document mdi-18px"></i>Pembayaran
     </a>
     <ul class="sub-menu children dropdown-menu">
      <li style="padding-left: 16px;" class="akad">
       <a href="<?php echo base_url() . 'payment' ?>"> Faktur Bayar</a>
       <a href="<?php echo base_url() . 'partial_payment' ?>"> Faktur Bayar Sebagian</a>
      </li>
     </ul>    
    </li>    
    
    <li class="menu-item-has-children dropdown">
     <a href="#" class="dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
      <i class="menu-icon mdi mdi-file-document mdi-18px"></i>Payroll
     </a>
     <ul class="sub-menu children dropdown-menu">
      <li style="padding-left: 16px;" class="akad">
       <a href="<?php echo base_url() . 'pegawai' ?>"> Pegawai</a>
       <a href="<?php echo base_url() . 'periode' ?>"> Periode</a>
       <a href="<?php echo base_url() . 'payroll_category' ?>"> Kategori</a>
       <a href="<?php echo base_url() . 'payroll' ?>"> Faktur Gaji</a>
      </li>
     </ul>    
    </li>    

    <li class="menu-item-has-children dropdown">
     <a href="#" class="dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
      <i class="menu-icon mdi mdi-account-settings-variant mdi-18px"></i>Setting
     </a>
     <ul class="sub-menu children dropdown-menu">
      <li style="padding-left: 16px;" class="pengaturan">
       <a href="<?php echo base_url() . 'pengguna/add' ?>"> Tambah User </a>
      </li>
      <li style="padding-left: 16px;" class="pengaturan">
       <a href="<?php echo base_url() . 'pengguna' ?>"> Data User </a>
      </li>
      <li style="padding-left: 16px;" class="pengaturan">
       <a href="<?php echo base_url() . 'general' ?>"> General </a>
      </li>
     </ul>    
    </li>    

    <li>
     <a href="<?php echo base_url() . 'tentang' ?>"> <i class="menu-icon mdi mdi-bookmark-outline mdi-18px"></i>Tentang </a>
    </li>    
    <li>
     <a href="<?php echo base_url() . 'bantuan' ?>" onclick="Template.showUpdateSystem(this, event)"> <i class="menu-icon mdi mdi-help-circle mdi-18px"></i>Bantuan </a>
    </li>    
    <li>
     <a href="<?php echo base_url() . 'apps' ?>"> <i class="menu-icon mdi mdi-apps mdi-18px"></i>Apps </a>
    </li>    
   </ul>
  </div><!-- /.navbar-collapse -->
 </nav>
</aside><!-- /#left-panel -->
