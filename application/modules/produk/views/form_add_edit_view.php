<div class="content">
 <div class="animated fadeIn">
  <div class="card">
   <div class="card-header">
    <div class="row">
     <div class="col-md-10">
      <div class="box-card-title middle-left">
       <i class="mdi mdi-clipboard-plus mdi-18px"></i><strong class="card-title"><?php echo isset($title_content) ? $title_content : '' ?></strong>
      </div>
     </div>
     <div class="col-sm-2 text-right"></div>
    </div>
   </div>
   <div class="card-body card-block">   
    <div class='row'>
     <div class='col-md-12'>
      <u>Data Produk Produk</u>
     </div>
    </div> 
    <hr/>
    <div class="row">
     <div class='col-md-3'>
      Tipe Produk
     </div>
     <div class='col-md-3'>
      <select id="tipe" error="Tipe" class="form-control required">
       <?php if (!empty($list_tipe)) { ?>
        <?php foreach ($list_tipe as $v_tip) { ?>
         <?php
         $selected = "";
         if (isset($tipe_product)) {
          if ($v_tip['id'] == $tipe_product) {
           $selected = 'selected';
          }
         }
         ?>
         <option <?php echo $selected ?> value="<?php echo $v_tip['id'] ?>"><?php echo $v_tip['tipe'] ?></option>
        <?php } ?>
       <?php } else { ?>
        <option value="">Tidak Ada Tipe Produk</option>
       <?php } ?>
      </select>
     </div>     
    </div>
    <br/>

    <div class='row'>
     <div class='col-md-3'>
      Nama Produk
     </div>
     <div class='col-md-3'>
      <input type='text' name='' id='product' class='form-control required' 
             error="Nama Produk" value='<?php echo isset($product) ? $product : '' ?>' placeholder=""/>
     </div>
    </div>
    <br/>

    <div class='row'>
     <div class='col-md-3'>
      Produk Kategori
     </div>
     <div class='col-md-3'>
      <select id="kategori" error="Produk Kategori" class="form-control required">
       <?php if (!empty($list_rk)) { ?>
        <?php foreach ($list_rk as $v_rk) { ?>
         <?php
         $selected = "";
         if (isset($kategori_product)) {
          if ($v_rk['id'] == $kategori_product) {
           $selected = 'selected';
          }
         }
         ?>
         <option <?php echo $selected ?> value="<?php echo $v_rk['id'] ?>"><?php echo $v_rk['kategori'] ?></option>
        <?php } ?>
       <?php } else { ?>
        <option value="">Tidak Ada Produk Kategori</option>
       <?php } ?>
      </select>
     </div>
    </div>
    <br/>


    <div class='row'>
     <div class='col-md-3'>
      Keterangan Produk
     </div>
     <div class='col-md-3'>
      <textarea class="form-control required" error="Detail Produk Produk" id="keterangan"><?php echo isset($keterangan) ? $keterangan : '' ?></textarea>
     </div>
    </div>
    <br/>

    <hr/>

    <div class='row'>
     <div class='col-md-3'>
      Harga Pokok Produk
     </div>
     <div class='col-md-3'>
      <input type='text' name='' id='harga_cash' class='form-control text-right required' 
             error="Harga Cash" value='<?php echo isset($harga_cash) ? $harga_cash : '0' ?>' placeholder=""/>
     </div>
    </div>
    <br/>

    <div class='row'>
     <div class='col-md-3'>
      Harga Jual Produk (Tunai)
     </div>
     <div class='col-md-3'>
      <input type='text' name='' id='harga_kredit' class='form-control text-right required' 
             error="Harga Kredit" value='<?php echo isset($harga_kredit) ? $harga_kredit : '0' ?>' placeholder=""/>
     </div>
    </div>
    <br/>

    <div class='row'>
     <div class='col-md-3'>
      <u>Foto Produk Produk</u>
     </div>
    </div>
    <br/>    

    <div class='row'>
     <?php if (isset($data_image)) { ?>
      <?php foreach ($data_image as $v_image) { ?>
       <div class='col-md-4' id='<?php echo $v_image['id'] ?>'>
        <div class='text-right hover' onclick="Produk.removeImage(this)">
         <i class="mdi mdi-close mdi-24px"></i>
        </div>
        <div class=''>
         <img src="<?php echo $v_image['foto'] ?>" width="300" height="300"/>
        </div>       
       </div>
      <?php } ?>
     <?php } ?>     
    </div>
    <br/>

    <div class='row'>
     <div class='col-md-12'>
      <div class="images">
       <div class="pic">
        add
       </div>
      </div>
     </div>
    </div>
    <br/>
    <hr/>

    <div class='row'>
     <div class='col-md-3'>
      <u>Detail Harga Angsuran</u>
     </div>
    </div>
    <br/>

    <div class='row'>
     <div class='col-md-12'>
      <table class="table table-striped table-bordered table-list-draft" id="list_ansuran">
       <thead>
        <tr>
         <th>No</th>
         <th>Harga Ansguran</th>
         <th>Tenor</th>
         <th>Harga Total</th>
         <th>&nbsp;</th>
        </tr>
       </thead>
       <tbody>
        <?php if (!isset($detail)) { ?>        
         <tr id="">         
          <td>1</td>
          <td>
           <input onkeyup="Produk.getHargaTotal(this, event)" type='text' name='' id='harga_angsuran' class='form-control text-right required harga_angsuran' 
                  value='0' placeholder="Harga Angsuran" error="Harga Angsuran"/>
          </td>
          <td class="text-center">
           <select onchange="Produk.getHargaTotal(this, event)" id="tenor" error="Tenor" class="form-control required">
            <?php if (!empty($list_tenor)) { ?>
             <?php foreach ($list_tenor as $v_t) { ?>
              <option total_ansuran="<?php echo $v_t['ansuran'] ?>" value="<?php echo $v_t['id'] ?>"><?php echo $v_t['periode_tahun'] ?></option>
             <?php } ?>
            <?php } else { ?>
             <option value="">Tenor Belum Diinputkan</option>
            <?php } ?>
           </select>
          </td>
          <td class="text-right"><?php echo isset($value['harga_total']) ? number_format($value['harga_total'], 2, ',', '.') : '0' ?></td>
          <td class="text-center">
           <i class="mdi mdi-plus mdi-24px" onclick="Produk.addDetail(this)"></i>
          </td>
         </tr>
        <?php } else { ?>
         <?php if (!empty($detail)) { ?>
          <?php $no = 1; ?>
          <?php foreach ($detail as $value) { ?>
           <tr class="edit" id="<?php echo $value['id'] ?>"> 
            <td><?php echo $no++ ?></td>
            <td>
             <input onkeyup="Produk.getHargaTotal(this, event)" type='text' name=''  id='harga_angsuran' class='form-control required' 
                    value='<?php echo $value['harga'] ?>' placeholder="Harga Angsuran" error="Harga Angsuran"/>
            </td>
            <td class="text-center">
             <select onchange="Produk.getHargaTotal(this, event)" id="tenor" error="Tenor" class="form-control required">
              <?php if (!empty($list_tenor)) { ?>
               <option value="">--Pilih Tenor--</option>
               <?php foreach ($list_tenor as $v_t) { ?>
                <option total_ansuran="<?php echo $v_t['ansuran'] ?>" value="<?php echo $v_t['id'] ?>" <?php echo $v_t['id'] == $value['ansuran'] ? 'selected' : '' ?>><?php echo $v_t['periode_tahun'] ?></option>
               <?php } ?>
              <?php } else { ?>
               <option value="">Tenor Belum Diinputkan</option>
              <?php } ?>
             </select>
            </td>
            <td class="text-right"><?php echo number_format($value['harga_total'], 2, ',', '.') ?></td>
           </tr>
          <?php } ?>
          <tr id="">         
           <td><?php echo $no ?></td>
           <td>
            <input onkeyup="Produk.getHargaTotal(this, event)" type='text' name='' id='harga_angsuran' class='form-control' 
                   value='' placeholder="Harga Angsuran"/>
           </td>
           <td class="text-center">
            <select onchange="Produk.getHargaTotal(this, event)" id="tenor" class="form-control">
             <?php if (!empty($list_tenor)) { ?>
              <option value="">--Pilih Tenor--</option>
              <?php foreach ($list_tenor as $v_t) { ?>
               <option total_ansuran="<?php echo $v_t['ansuran'] ?>" value="<?php echo $v_t['id'] ?>"><?php echo $v_t['periode_tahun'] ?></option>
              <?php } ?>
             <?php } else { ?>
              <option value="">Tenor Belum Diinputkan</option>
             <?php } ?>
            </select>
           </td>
           <td class="text-right">0</td>
           <td class="text-center">
            <i class="mdi mdi-plus mdi-24px" onclick="Produk.addDetail(this)"></i>
           </td>           
          </tr>
         <?php } ?>
        <?php } ?>
       </tbody>
      </table>
     </div>
    </div>
    <hr/>

    <div class='row'>
     <div class='col-md-12 text-right'>
      <button id="" class="btn btn-succes-baru" onclick="Produk.simpan('<?php echo isset($id) ? $id : '' ?>')">Simpan</button>
      &nbsp;
      <button id="" class="btn btn-baru" onclick="Produk.back()">Kembali</button>
     </div>
    </div>
   </div>
  </div>
 </div>
</div>
