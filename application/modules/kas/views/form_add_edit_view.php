<input type='hidden' name='' id='id' class='form-control' value='<?php echo isset($id) ? $id : '' ?>'/>
<div class="content">
 <div class="animated fadeIn">
  <div class="card">
   <div class="card-header">
    <div class="row">
     <div class="col-md-10">
      <div class="box-card-title middle-left">
       <i class="mdi mdi-clipboard-plus mdi-18px"></i><strong class="card-title"><?php echo isset($title_content) ? $title_content : '' ?></strong>
      </div>
     </div>
     <div class="col-sm-2 text-right"></div>
    </div>
   </div>
   <div class="card-body card-block">   
    <div class='row'>
     <div class='col-md-12'>
      <u>Data Kas</u>
     </div>
    </div> 
    <hr/>
    <div class="row">
     <div class='col-md-3'>
      Keterangan
     </div>
     <div class='col-md-3'>
      <input type='text' name='' id='keterangan' class='form-control required' 
             value='<?php echo isset($keterangan) ? $keterangan : '' ?>' error="Keterangan"/>
     </div>     
    </div>
    <br/>
    
    <div class="row">
     <div class='col-md-3'>
      Jumlah
     </div>
     <div class='col-md-3'>
      <input type='text' name='' id='jumlah' class='form-control text-right required' 
             value='<?php echo isset($jumlah) ? $jumlah : '0' ?>' error="Jumlah"/>
     </div>     
    </div>
    <br/>
    <hr/>
    <div class='row'>
     <div class='col-md-12 text-right'>
      <button id="" class="btn btn-succes-baru" onclick="Kas.simpan('<?php echo isset($id) ? $id : '' ?>')">Simpan</button>
      &nbsp;
      <button id="" class="btn btn-baru" onclick="Kas.back()">Kembali</button>
     </div>
    </div>
   </div>
  </div>
 </div>
</div>
