<input type='hidden' name='' id='id' class='form-control' value='<?php echo isset($id) ? $id : '' ?>'/>
<div class="content">
 <div class="animated fadeIn">
  <div class="card">
   <div class="card-header">
    <div class="row">
     <div class="col-md-10">
      <div class="box-card-title middle-left">
       <i class="mdi mdi-clipboard-plus mdi-18px"></i><strong class="card-title"><?php echo isset($title_content) ? $title_content : '' ?></strong>
      </div>
     </div>
     <div class="col-sm-2 text-right">      
     </div>
    </div>
   </div>
   <div class="card-body card-block">   
    <div class='row'>
     <div class='col-md-8'>
      <u>Data Faktur</u>
     </div>
    </div> 
    <hr/>
    <div class="row">
     <div class='col-md-3'>
      No Faktur
     </div>
     <div class='col-md-3'>
      <?php echo $no_faktur ?>
     </div>     
    </div>
    <br/>

    <div class="row">
     <div class='col-md-3'>
      Pegawai
     </div>
     <div class='col-md-3'>
      <?php echo $nama_pegawai ?>
     </div>     
    </div>
    <br/>

    <div class="row">
     <div class='col-md-3'>
      Tanggal Faktur
     </div>
     <div class='col-md-3'>
      <?php echo $tanggal_faktur ?>
     </div>     
    </div>
    <br/>
    <div class="row">
     <div class='col-md-3'>
      Tanggal Bayar
     </div>
     <div class='col-md-3'>
      <?php echo $tanggal_bayar ?>
     </div>     
    </div>
    <br/>        
    <div class="row">
     <div class='col-md-3'>
      Periode
     </div>
     <div class='col-md-3'>
      <?php echo $month_str . $year ?>
     </div>     
    </div>
    <br/>        
    <div class="row">
     <div class='col-md-3'>
      Keterangan
     </div>
     <div class='col-md-3'>
      <?php echo $keterangan ?>
     </div>     
    </div>
    <br/>        
    <hr/>

    <div class="row">
     <div class="col-md-12">
      <u>Data Gaji</u>
     </div>
    </div>
    <br/>

    <div class="row">
     <div class="col-md-10">
      <div class="table-responsive">
       <table class="table table-striped table-bordered table-list-draft" id="tb_product">
        <thead>
         <tr>
          <th>Kategori</th>
          <th>Jumlah</th>
          <th>Keterangan</th>
         </tr>
        </thead>
        <tbody>
         <?php if (!empty($payroll_item)) { ?>
          <?php foreach ($payroll_item as $value) { ?>
           <tr> 
            <td><?php echo $value['jenis'] ?></td>
            <td><?php echo 'Rp, ' . number_format($value['jumlah']) ?></td>
            <td><?php echo $value['keterangan'] ?></td>
           </tr>
          <?php } ?>
         <?php } ?>       
        </tbody>
       </table>
      </div>
     </div>
    </div>

    <div class="row">
     <div class="col-md-10 text-right">
      <h4>Total : Rp, <label id="total"><?php echo number_format($total) ?></label></h4>
     </div>
    </div>
    <br/>
    <div class='row'>
     <div class='col-md-12 text-right'>
      <button id="" class="btn btn-danger-baru" onclick="Payroll.cetak('<?php echo isset($id) ? $id : '' ?>')">Cetak</button>
      &nbsp;
      <button id="" class="btn btn-baru" onclick="Payroll.back()">Kembali</button>
     </div>
    </div>
   </div>
  </div>
 </div>
</div>
