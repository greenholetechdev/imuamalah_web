<input type='hidden' name='' id='id' class='form-control' value='<?php echo isset($id) ? $id : '' ?>'/>
<div class="content">
 <div class="animated fadeIn">
  <div class="card">
   <div class="card-header">
    <div class="row">
     <div class="col-md-10">
      <div class="box-card-title middle-left">
       <i class="mdi mdi-clipboard-plus mdi-18px"></i><strong class="card-title"><?php echo isset($title_content) ? $title_content : '' ?></strong>
      </div>
     </div>
     <div class="col-sm-2 text-right"></div>
    </div>
   </div>
   <div class="card-body card-block">   
    <div class='row'>
     <div class='col-md-12'>
      <u>Data Notif</u>
     </div>
    </div> 
    <hr/>
    <div class="row">
     <div class='col-md-3'>
      Nama Notif
     </div>
     <div class='col-md-3'>
      <?php echo $nama_notif ?>
     </div>     
    </div>
    <br/>

    <div class="row">
     <div class='col-md-3'>
      Jadwal Notif
     </div>
     <div class='col-md-3'>
      <?php echo $jadwal_notif ?>
     </div>     
    </div>
    <br/>

    <div class="row">
     <div class='col-md-3'>
      Jenis Notif
     </div>
     <div class='col-md-3'>
      <?php echo $jenis_notif ?>
     </div>     
    </div>
    <br/>

    <div class="row">
     <div class='col-md-4'>
      <u>Jenis Pengiriman</u>
     </div>
    </div>
    <br/>

    <div class="row">
     <div class='col-md-12'>
      <table class="table table-striped table-bordered table-list-draft">
       <thead>
        <tr>
         <th>Pengiriman</th>
         <th>Action</th>
        </tr>
       </thead>
       <tbody>
        <?php foreach ($jenis_pengiriman as $v_p) { ?>
         <tr>
          <td><?php echo $v_p['jenis'] ?></td>
          <td>
           <?php $check = ""; ?>
           <?php $id = ""; ?>
           <?php if (isset($detail)) { ?>
            <?php foreach ($detail as $v_d) { ?>
             <?php if ($v_d['jenis_notif_pengiriman'] == $v_p['id']) { ?>
              <?php $check = "checked" ?>
              <?php $id = $v_d['id']; ?>
             <?php } ?>
            <?php } ?>
           <?php } ?>
           <input disabled <?php echo $check ?> id_jalur="<?php echo $id ?>" <?php echo $v_p['status'] == 1 ? '' : 'disabled' ?> type='checkbox' name='' id='check' class='form-control' value='<?php echo $v_p['id'] ?>'/>
          </td>
         </tr>
        <?php } ?>
       </tbody>
      </table>

     </div>
    </div>
    <br/>
    <hr/>
    <div class='row'>
     <div class='col-md-12 text-right'>
      <button id="" class="btn btn-baru" onclick="JatuhTempo.back()">Kembali</button>
     </div>
    </div>
   </div>
  </div>
 </div>
</div>
