<?php

class Pembelian_produk extends MX_Controller {

 public $segment;
 public $limit;
 public $page;
 public $last_no;

 public function __construct() {
  parent::__construct();
  $this->limit = 10;
 }

 public function getModuleName() {
  return 'pembelian_produk';
 }

 public function getHeaderJSandCSS() {
  $data = array(
      '<script src="' . base_url() . 'assets/js/bootbox.js"></script>',
      '<script src="' . base_url() . 'assets/js/controllers/pembelian_produk.js"></script>'
  );

  return $data;
 }

 public function getTableName() {
  return 'pembeli_has_product';
 }

 public function index() {
  $this->segment = 3;
  $this->page = $this->uri->segment($this->segment) ?
          $this->uri->segment($this->segment) - 1 : 0;
  $this->last_no = $this->page * $this->limit;

  $data['view_file'] = 'index_view';
  $data['header_data'] = $this->getHeaderJSandCSS();
  $data['module'] = $this->getModuleName();
  $data['title'] = "Data Pembelian";
  $data['title_content'] = 'Data Pembelian';
  $content = $this->getDataPembelian();
  $data['content'] = $content['data'];
  $total_rows = $content['total_rows'];
  $data['pagination'] = Modules::run('pagination/get_pagination', $this->getModuleName() . '/index/', $this->segment, $total_rows, $this->limit, $this->last_no);
  echo Modules::run('template', $data);
 }

 public function getTotalDataPembelian($keyword = '') {
  $like = array();
  if ($keyword != '') {
   $like = array(
       array('p.no_invoice', $keyword),
   );
  }
  $total = Modules::run('database/count_all', array(
              'table' => 'pembelian p',
              'field' => array('p.*'),
              'like' => $like,
              'is_or_like' => true,
  ));

  return $total;
 }

 public function getDataCustomer($pembelian) {
  $data = Modules::run('database/get', array(
              'table' => 'pembeli_has_product phr',
              'field' => array('p.nama'),
              'join' => array(
                  array('pembeli p', 'phr.pembeli = p.id')
              ),
              'where' => array('phr.pembelian' => $pembelian)
          ))->row_array();

  $customer = $data['nama'];
  return $customer;
 }

 public function getDataProdukDibeli($pembelian) {
  $data = Modules::run('database/get', array(
              'table' => 'pembeli_has_product phr',
              'field' => array('r.product'),
              'join' => array(
                  array('pembeli p', 'phr.pembeli = p.id'),
                  array('product r', 'phr.product = r.id'),
              ),
              'where' => array('phr.pembelian' => $pembelian)
  ));

  $result = array();
  if (!empty($data)) {
   foreach ($data->result_array() as $value) {
    $result[] = $value['product'];
   }
  }

  return implode('<br/>', $result);
 }

 public function getDataPembelian($keyword = '') {
  $like = array();
  if ($keyword != '') {
   $like = array(
       array('p.no_invoice', $keyword),
   );
  }
  $data = Modules::run('database/get', array(
              'table' => 'pembelian p',
              'field' => array('p.*'),
              'like' => $like,
              'is_or_like' => true,
  ));

  $result = array();
  if (!empty($data)) {
   foreach ($data->result_array() as $value) {
    $value['nama_customer'] = $this->getDataCustomer($value['id']);
    $value['nama_product'] = $this->getDataProdukDibeli($value['id']);
    array_push($result, $value);
   }
  }

//  echo '<pre>';
//  print_r($result);die;
  return array(
      'data' => $result,
      'total_rows' => $this->getTotalDataPembelian($keyword)
  );
 }

 public function getDetailDataPembelian($id) {
  $data = Modules::run('database/get', array(
              'table' => $this->getTableName() . ' phr',
              'field' => array('phr.*', 'r.product as nama',
                  'p.nama as nama_customer', 'p.no_hp', 'p.alamat', 'pm.jatuh_tempo', 'pm.no_invoice as no_pembelian'),
              'join' => array(
                  array('product r', 'r.id = phr.product'),
                  array('pembeli p', 'phr.pembeli = p.id'),
                  array('pembelian pm', 'phr.pembelian = pm.id'),
              ),
              'where' => "pm.id = '" . $id . "'"
  ));

//  echo '<pre>';
//  print_r($data->row_array());
//  die;
  return $data->row_array();
 }

 public function getListCustomer() {
  $data = Modules::run('database/get', array(
              'table' => 'pembeli',
              'where' => "deleted is null or deleted = 0"
  ));

  $result = array();
  if (!empty($data)) {
   foreach ($data->result_array() as $value) {
    array_push($result, $value);
   }
  }


  return $result;
 }

 public function getCustomer($id) {
  $data = Modules::run('database/get', array(
              'table' => 'pembeli',
          ))->row_array();

  $content['data'] = $this->getCustomerIdentitas($id);

  $data['view'] = $this->load->view('list_identitas', $content, true);
  echo json_encode($data);
 }

 public function getListIDentitasData() {
  $content['data'] = array();
  $content['list_identitas'] = $this->getListIdentitas();
  echo $this->load->view('list_identitas', $content, true);
 }

 public function getListStatusPembelian() {
  $data = Modules::run('database/get', array(
              'table' => 'status_pembelian',
  ));

  $result = array();
  if (!empty($data)) {
   foreach ($data->result_array() as $value) {
    array_push($result, $value);
   }
  }


  return $result;
 }

 public function getListIdentitas() {
  $data = Modules::run('database/get', array(
              'table' => 'indetitas',
  ));

  $result = array();
  if (!empty($data)) {
   foreach ($data->result_array() as $value) {
    array_push($result, $value);
   }
  }


  return $result;
 }

 public function add() {
  $data['view_file'] = 'form_add_edit_view';
  $data['header_data'] = $this->getHeaderJSandCSS();
  $data['module'] = $this->getModuleName();
  $data['title'] = "Tambah Pembelian";
  $data['title_content'] = 'Tambah Pembelian';
  $data['list_customer'] = $this->getListCustomer();
  $data['list_status'] = $this->getListStatusPembelian();
  $data['list_identitas'] = $this->getListIdentitas();
  $data['list_syarat'] = $this->getListPersyaratan();
  $data['data_product'] = Modules::run('produk/getDataProduk')['data'];
//  echo '<pre>';
//  print_r($data);die;
  echo Modules::run('template', $data);
 }

 public function getDetailSyarat($persyaratan) {
  $data['detail'] = Modules::run('database/get', array(
              'table' => 'persyaratan_has_berkas phb',
              'field' => array('phb.*'),
              'where' => array('phb.persyaratan' => $persyaratan)
          ))->result_array();

  echo $this->load->view('detail_persyaratan', $data, true);
 }

 public function getCustomerIdentitas($pembeli) {
  $data = Modules::run('database/get', array(
              'table' => 'pembeli_has_identitas phi',
              'field' => array('phi.*', 'i.identitas'),
              'join' => array(
                  array('indetitas i', 'phi.indetitas = i.id')
              ),
              'where' => array('phi.pembeli' => $pembeli)
  ));

  $result = array();
  if (!empty($data)) {
   foreach ($data->result_array() as $value) {
    array_push($result, $value);
   }
  }


  return $result;
 }

 public function getProdukDibeliCustomer($id) {
  $data = Modules::run('database/get', array(
              'table' => $this->getTableName() . ' phr',
              'field' => array('phr.*',
                  'r.product as nama', 'tr.tipe',
                  'kr.kategori', 'sp.status', 'rhhjc.harga as harga_cash',
                  'rhhjk.harga as harga_kredit', 'pm.no_invoice as no_pembelian'),
              'join' => array(
                  array('product r', 'phr.product = r.id'),
                  array('tipe_product tr', 'r.tipe_product = tr.id'),
                  array('kategori_product kr', 'r.kategori_product = kr.id'),
                  array('status_pembelian sp', 'phr.status_pembelian = sp.id'),
                  array('product_has_harga_jual_pokok rhhjc', 'rhhjc.product = r.id and rhhjc.period_end is null'),
                  array('product_has_harga_jual_tunai rhhjk', 'rhhjk.product = r.id and rhhjk.period_end is null'),
                  array('pembelian pm', 'phr.pembelian = pm.id'),
              ),
              'where' => array('pm.id' => $id)
  ));

  $result = array();
  if (!empty($data)) {
   foreach ($data->result_array() as $value) {
    $value['detail_angsuran'] = $this->getPembelianProdukAngsuran($value['id']);
    array_push($result, $value);
   }
  }


  return $result;
 }

 public function getPembelianProdukAngsuran($id) {
  $data = Modules::run('database/get', array(
              'table' => 'pembeli_has_angsuran pha',
              'field' => array('pha.*',
                  'rhha.harga as harga_angsuran',
                  'a.periode_tahun', 'a.ansuran as ansuran_periode', 'rhha.harga_total'),
              'join' => array(
                  array('product_has_harga_angsuran rhha', 'pha.product_has_harga_angsuran = rhha.id and rhha.period_end is null'),
                  array('ansuran a', 'rhha.ansuran = a.id'),
              ),
              'where' => array('pha.pembeli_has_product' => $id)
  ));

  $result = array();
  if (!empty($data)) {
   foreach ($data->result_array() as $value) {
    array_push($result, $value);
   }
  }


  return $result;
 }

 public function getDataPersyaratanPembelian($id) {
  $data = Modules::run('database/get', array(
              'table' => 'pembeli_has_persyaratan php',
              'field' => array('php.*', 'phb.nama_berkas', 'p.syarat'),
              'join' => array(
                  array('persyaratan_has_berkas phb', 'php.persyaratan_has_berkas = phb.id'),
                  array('persyaratan p', 'phb.persyaratan = p.id')
              ),
              'where' => array('php.pembelian' => $id)
  ));

  $result = array();
  if (!empty($data)) {
   foreach ($data->result_array() as $value) {
    array_push($result, $value);
   }
  }


  return $result;
 }

 public function detail($id) {
//  echo $id;die;
  $data = $this->getDetailDataPembelian($id);  
  $data['detail_product'] = $this->getProdukDibeliCustomer($id);
  $data['view_file'] = 'detail_view';
  $data['header_data'] = $this->getHeaderJSandCSS();
  $data['module'] = $this->getModuleName();
  $data['title'] = "Detail Pembelian";
  $data['title_content'] = 'Detail Pembelian';
  $data['detail_syarat'] = $this->getDataPersyaratanPembelian($id);
  echo Modules::run('template', $data);
 }

 public function getPostDataCustomer($value) {
//  $data['pembelian'] = $pembelian;
  $data['nama'] = $value->nama;
  $data['no_hp'] = $value->no_hp;
  $data['alamat'] = $value->alamat;
  $data['email'] = $value->email;
  return $data;
 }

 public function getPostDataCustomerIdentitas($value, $pembeli) {
  $data['pembeli'] = $pembeli;
  $data['indetitas'] = $value->identitas;
  $data['no_identitas'] = $value->no_identitas;
  return $data;
 }

 public function getPostDataCustomerHasAngsuran($value, $pembeli_has) {
  $data['pembeli_has_product'] = $pembeli_has;
  $data['product_has_harga_angsuran'] = $value->angsuran;
  return $data;
 }

 public function getPostDataCustomerHasProduk($value, $pembeli, $pembelian) {
  $data['pembelian'] = $pembelian;
  $data['pembeli'] = $pembeli;
  $data['no_invoice'] = Modules::run('no_generator/generateInvoice', 'INVPR', 'pembeli_has_product');
  $data['product'] = $value->product;
  $data['status_pembelian'] = $value->status_pembelian;
  $data['tgl_beli'] = date('Y-m-d');
  return $data;
 }

 public function getPostDataPembelian() {
  $data['no_invoice'] = Modules::run('no_generator/generateInvoice', 'PEM', 'pembelian');
  return $data;
 }

 public function proses() {
  $data = json_decode($this->input->post('data'));
//  echo '<pre>';
//  print_r($data);die;
//  echo '<pre>';
//  print_r($_FILES);
//  die;
  $id = $this->input->post('id');
  $is_valid = false;
  $pembelian = $id;

  $is_save = true;
  if ($is_save) {
   $this->db->trans_begin();
   try {
    $post_pembelian = $this->getPostDataPembelian();
    $post_pembelian['jatuh_tempo'] = $data->jatuh_tempo;
    $pembelian = Modules::run('database/_insert', 'pembelian', $post_pembelian);
    $post_customer = $this->getPostDataCustomer($data);
    if ($data->customer == 'baru') {
     //insert indentitas
     $pembeli = Modules::run('database/_insert', 'pembeli', $post_customer);
     //insert customer has_identitas
//    $post_customer_identitas = $this->getPostDataCustomerIdentitas($data, $pembeli);
//    Modules::run('database/_insert', 'pembeli_has_identitas', $post_customer_identitas);
    } else {
     $pembeli = $data->customer;
    }

    //insert pembeli_has
//    echo '<pre>';
//    print_r($data->detail);die;
    foreach ($data->detail as $value) {
     $post_customer_has = $this->getPostDataCustomerHasProduk($value, $pembeli, $pembelian);
//     echo '<pre>';
//     print_r($post_customer_has);die;
     $pembeli_has = Modules::run('database/_insert', 'pembeli_has_product', $post_customer_has);
//     echo "<pre>";
//     echo $this->db->last_query();
//     die;
//     echo $pembeli_has;die;
     if (isset($value->angsuran)) {
      if ($value->angsuran != '') {
       //insert pembeli_has angsuran
       $post_customer_has_angsuran = $this->getPostDataCustomerHasAngsuran($value, $pembeli_has);
       Modules::run('database/_insert', 'pembeli_has_angsuran', $post_customer_has_angsuran);
      }
     }
    }

    //Detail Syarat
    $i = 1;
    $post_pembeli_has_syarat = array();
    foreach ($data->detail_syarat as $v_s) {
     if ($v_s->status == 'Y') {
      $nama_berkas = $_FILES['berkas' . $i];
      $post_pembeli_has_syarat['berkas'] = $nama_berkas['name'];
      $this->uploadData('berkas' . $i);
      $i += 1;
     } else {
      unset($post_pembeli_has_syarat['berkas']);
     }
     $post_pembeli_has_syarat['pembelian'] = $pembelian;
     $post_pembeli_has_syarat['persyaratan_has_berkas'] = $v_s->id;
     Modules::run('database/_insert', 'pembeli_has_persyaratan', $post_pembeli_has_syarat);
    }

    $this->db->trans_commit();
    $is_valid = true;
   } catch (Exception $ex) {
    $this->db->trans_rollback();
   }
  }


  echo json_encode(array('is_valid' => $is_valid, 'pembelian' => $pembelian));
 }

 public function uploadData($name_of_field) {
  $config['upload_path'] = 'files/berkas/persyaratan/';
  $config['allowed_types'] = 'gif|jpg|png|pdf';
  $config['max_size'] = '1000';
  $config['max_width'] = '2000';
  $config['max_height'] = '2000';

  $this->load->library('upload', $config);
  $this->upload->do_upload($name_of_field);
 }

 public function search($keyword) {
  $this->segment = 4;
  $this->page = $this->uri->segment($this->segment) ?
          $this->uri->segment($this->segment) - 1 : 0;
  $this->last_no = $this->page * $this->limit;
  $keyword = urldecode($keyword);

  $data['keyword'] = $keyword;
  $data['view_file'] = 'index_view';
  $data['header_data'] = $this->getHeaderJSandCSS();
  $data['module'] = $this->getModuleName();
  $data['title'] = "Data Pembelian";
  $data['title_content'] = 'Data Pembelian';
  $content = $this->getDataPembelian($keyword);
  $data['content'] = $content['data'];
  $total_rows = $content['total_rows'];
  $data['pagination'] = Modules::run('pagination/get_pagination', $this->getModuleName() . '/index/', $this->segment, $total_rows, $this->limit, $this->last_no);
  echo Modules::run('template', $data);
 }

 public function getDataPembelianSatuanHarga($produk_satuan) {
  $data = Modules::run('database/get', array(
              'table' => 'produk_satuan_has_harga',
              'where' => array('produk_has_satuan' => $produk_satuan)
  ));

  $result = array();
  if (!empty($data)) {
   foreach ($data->result_array() as $value) {
    $value['active'] = "0";
    if ($value['period_end'] == '') {
     $value['active'] = "1";
     $value['period_end'] = '-';
    } else {
     $value['period_end'] = date('d F Y', strtotime($value['period_end']));
    }
    $value['period_start'] = date('d F Y', strtotime($value['period_start']));
    array_push($result, $value);
   }
  }


  return $result;
 }

 public function getListPersyaratan() {
  $data = Modules::run('database/get', array(
              'table' => 'persyaratan',
//  'where' => array('kategori_akad' => 2)
  ));

  $result = array();
  if (!empty($data)) {
   foreach ($data->result_array() as $value) {
    array_push($result, $value);
   }
  }


  return $result;
 }

 public function getAngsuran($product) {
  $data['data'] = Modules::run('produk/getDetailHargaAnsuran', $product);
  $data['list_syarat'] = $this->getListPersyaratan();
  echo $this->load->view('data_ansguran', $data, true);
 }

 public function detailSyarat($id) {
  $data = Modules::run('persyaratan/getDetailDataPersyaratan', $id);
  $data['detail'] = Modules::run('persyaratan/getDetailBerkasPersyaratan', $id);
  ;
  $data['title'] = "Detail Persyaratan";
  $data['title_content'] = 'Detail Persyaratan';

  echo $this->load->view('detail_persyaratan', $data, true);
 }

 public function detailProduk($id) {
  $data = Modules::run('produk/getDetailDataProduk', $id);
  $data['detail'] = Modules::run('produk/getDetailHargaAnsuran', $id);
  $data['title'] = "Detail Produk";
  $data['title_content'] = 'Detail Produk';
  $data['data_image'] = Modules::run('produk/getDataImageProduk', $id);
//  echo '<pre>';
//  print_r($data);die;
  echo $this->load->view('detail_product', $data, true);
 }

}
