<input type='hidden' name='' id='id' class='form-control' value='<?php echo isset($id) ? $id : '' ?>'/>
<div class="content">
 <div class="animated fadeIn">
  <div class="card">
   <div class="card-header">
    <div class="row">
     <div class="col-md-10">
      <div class="box-card-title middle-left">
       <i class="mdi mdi-clipboard-plus mdi-18px"></i><strong class="card-title"><?php echo isset($title_content) ? $title_content : '' ?></strong>
      </div>
     </div>
     <div class="col-sm-2 text-right"></div>
    </div>
   </div>
   <div class="card-body card-block">   
    <div class='row'>
     <div class='col-md-12'>
      <u>Data Customer</u>
     </div>
    </div> 
    <hr/>

    <div class='row'>
     <div class='col-md-3'>
      No. Pembelian
     </div>     
     <div class='col-md-3'>
      <?php echo $no_pembelian ?>
     </div>     
    </div>
    <br/>

    <div class='data_customer'>
     <div class="row">
      <div class='col-md-3'>
       Nama Customer
      </div>
      <div class='col-md-3'>
       <?php echo $nama_customer ?>
      </div>     
     </div>
     <br/>

     <div class="row">
      <div class='col-md-3'>
       No HP
      </div>
      <div class='col-md-3'>
       <?php echo $no_hp ?>
      </div>     
     </div>
     <br/>

     <div class="row">
      <div class='col-md-3'>
       Alamat
      </div>
      <div class='col-md-3'>
       <?php echo $alamat ?>
      </div>     
     </div>
     <br/>
    </div>
    <hr/>

    <div class="row">
     <div class='col-md-12'>
      <u>Produk yang Dibeli</u>
     </div>
    </div>
    <br/>

    <div class='row'>
     <div class='col-md-12'>
      <div class='table-responsive'>
       <table class="table table-striped table-bordered table-list-draft" id="list_product_taken">
        <thead>
         <tr>
          <th>Kode Pembelian</th>
          <th>Produk</th>
          <th>Tipe</th>
          <th>Kategori</th>
          <th>Harga Pokok Produk</th>
          <th>Harga Jual Produk (Tunai)</th>
          <th>Jenis Pembelian</th>
         </tr>
        </thead>
        <tbody>
         <?php if (!empty($detail_product)) { ?>
          <?php foreach ($detail_product as $value) { ?>
           <tr class="" id='<?php echo $value['id'] ?>'>
            <td><?php echo $value['no_invoice'] ?></td>
            <td><?php echo $value['product'] ?></td>
            <td><?php echo $value['tipe'] ?></td>
            <td><?php echo $value['kategori'] ?></td>
            <td><?php echo 'Rp. ' . number_format($value['harga_cash'], 2, ',', '.') ?></td>
            <td><?php echo 'Rp. ' . number_format($value['harga_kredit'], 2, ',', '.') ?></td>
            <td class="text-center">
             <?php echo $value['status'] ?>
             &nbsp;
             <i class="mdi mdi-chevron-double-down mdi-18px hover" onclick="PembelianProduk.showAnsuran(this)"></i>
            </td>
           </tr>
           <?php if (!empty($value['detail_angsuran'])) { ?>
            <?php foreach ($value['detail_angsuran'] as $v_a) { ?>
             <tr class="detail_ansuran<?php echo $value['id'] ?>">
              <td colspan="7" style="padding: 16px;">
               * Periode Angsuran : <b><?php echo $v_a['periode_tahun'] ?></b>
               <br/>
               * Total Angsuran   : <b><?php echo $v_a['ansuran_periode'] ?> x</b>
               <br/>
               * Jumlah Angsuran  : <b>Rp. <?php echo number_format($v_a['harga_angsuran'], 2, ',', '.') ?> / Bulan</b>              
               <br/>
               * Total Harga  : <b>Rp. <?php echo number_format($v_a['harga_total'], 2, ',', '.') ?></b>
               <hr/>
               <br/>
              </td>
             </tr>
            <?php } ?>
           <?php } else { ?>
            <tr>
             <td colspan="6">Tidak Ada Angsuran</td>
            </tr>
           <?php } ?>
          <?php } ?>
         <?php } ?>        
        </tbody>
       </table>
      </div>
     </div>
    </div>
    <br/>

    <div class="row">
     <div class='col-md-12'>
      <u>Persyaratan Pembelian</u>
     </div>
    </div>
    <br/>

    <div class="row">
     <div class='col-md-4'>
      <?php echo '-> ' . $detail_syarat[0]['syarat'] ?>
     </div>
    </div>
    <br/>

    <div class="row">
     <div class='col-md-12'>
      <u>Tanggal Jatuh Tempo</u>
     </div>
    </div>
    <br/>

    <div class="row">
     <div class='col-md-4'>
      <?php echo '-> Tanggal : ' . $jatuh_tempo ?>
     </div>
    </div>
    <br/>

    <div class='row'>
     <div class='col-md-12'>
      <div class='table-responsive'>
       <table class="table table-striped table-bordered table-list-draft" id="">
        <thead>
         <tr>
          <th>Nama Syarat</th>
          <th>Upload Berkas</th>
         </tr>
        </thead>
        <tbody class="persyaratan">
         <?php if (!empty($detail_syarat)) { ?>
          <?php foreach ($detail_syarat as $v_s) { ?>
           <tr>
            <td><?php echo $v_s['nama_berkas'] ?></td>
            <td><?php echo $v_s['berkas'] ?></td>
           </tr>
          <?php } ?>
         <?php } ?>
        </tbody>
       </table>
      </div>
     </div>
    </div>
    <br/>

    <div class='row'>
     <div class='col-md-12 text-right'>
      <button id="" class="btn btn-baru" onclick="PembelianProduk.back()">Kembali</button>
     </div>
    </div>
   </div>
  </div>
 </div>
</div>
