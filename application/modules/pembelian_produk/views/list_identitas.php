<?php if (!empty($data)) { ?>
 <?php foreach ($data as $value) { ?>
  <div class="row">
   <div class='col-md-3'>
    Pilih Identitas
   </div>
   <div class='col-md-3'>
    <select disabled id="identitas" error="Identitas" class="form-control required">
     <option value="<?php echo $value['indetitas'] ?>"><?php echo $value['identitas'] ?></option>
    </select>
   </div>     
  </div>
  <br/>
  <div class="row">
   <div class='col-md-3'>
    Nomor Identitas
   </div>
   <div class='col-md-3'>
    <input type='text' name='' id='no_identitas' class='form-control required' value='<?php echo $value['no_identitas'] ?>' error="Nomer Identitas" disabled/>
   </div>     
  </div>
  <br/>
 <?php } ?>
<?php } else { ?>
 <div class="row">
  <div class='col-md-3'>
   Pilih Identitas
  </div>
  <div class='col-md-3'>
   <select id="identitas" error="Identitas" class="form-control required">
    <?php if (!empty($list_identitas)) { ?>
     <?php foreach ($list_identitas as $v_den) { ?>
      <option value="<?php echo $v_den['id'] ?>"><?php echo $v_den['identitas'] ?></option>
     <?php } ?>
    <?php } ?>
   </select>
  </div>     
 </div>
 <br/>
 <div class="row">
  <div class='col-md-3'>
   Nomor Identitas
  </div>
  <div class='col-md-3'>
   <input type='text' name='' id='no_identitas' class='form-control required' value='' error="Nomer Identitas"/>
  </div>     
 </div>
 <br/>
<?php } ?>

